import { React, useEffect, useState, Fragment } from "react";
import { Auth } from "aws-amplify";
import backgroundImage from "../../../../src/assets/images/icons/badge-bg.png";
import {
  Heart,
  MessageSquare,
  MoreHorizontal,
  Filter,
  X,
  Check,
  Eye,
  Share,
} from "react-feather";
import {
  AvForm,
  AvField,
  AvInput,
  AvGroup,
} from "availity-reactstrap-validation-safe";
import { getCompany } from "./../../../redux/actions/company/index";
import { getUser } from "../../../redux/actions/user";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Multiselect } from "multiselect-react-dropdown";
import userIcon from "@src/assets/images/avatars/icon_user.png";
import { AmplifyS3Image } from "@aws-amplify/ui-react";
import moment from "moment";
import { Spinner, CardBody } from "reactstrap";
import Avatar from "@components/avatar";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import CompanyNews from "../../commonFiles/CompanyNews";
import CompanyAnnouncement from "../../commonFiles/companyAnnouncementCard";

//importing actions
import {
  createBadgeAction,
  listBadgeImagesAction,
  addBadgesCommentAction,
  badgeCommentLikeAction,
  deleteBadgeCommentAction,
  likeBadgesAction,
  listAllBadgesAction,
  listBadgeAction,
  updateBadgeCommentAction,
  deleteBadgeLikeAction,
  deleteBadgeCommentLikeAction,
} from "../../../redux/actions/badge";

import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
const ListAllBadges = (props) => {
  const [userID, setUserID] = useState("");
  const [userProfileImage, setUserProfileImage] = useState(null);
  const [userRole, setUserRole] = useState("");
  const [sendingBadgeUser, setSendingBadgeUser] = useState([]);
  const [description, setDescription] = useState(null);
  const [companyID, setCompanyID] = useState("");
  const [userName, setUserName] = useState("");
  const [img, setImg] = useState();
  const [badgeOwnerID, setBadgeOwnerID] = useState("");
  const [commentContent, setcommentContent] = useState("");
  const [commentReply, setcommentReply] = useState("");
  const [numberLikes, setnumberLikes] = useState("");
  const [CommentError, setCommentError] = useState(false);
  const [loadProcess, setLoadProcess] = useState(false);
  const [read, setRead] = useState(false);
  const [index1, setIndex1] = useState(null);
  const [EditCommentMode, setEditCommentMode] = useState(false);
  const [EditCommentData, setEditCommentData] = useState(null);
  const [UpdatedComment, setUpdatedComment] = useState(null);
  const [likeinfo, setlikeInfo] = useState([]);
  const [commentReplyNumber, setCommentReplyNumber] = useState(null);
  const [badgeNumber, setBadgeNumber] = useState(null);
  const [editCommentInfo, setEditCommentInfo] = useState(null);
  const [EditFlag, setEditFlag] = useState(false);
  const [commentReplyContent, setcommentReplyContent] = useState("");
  const [loadSaveChangesProcess, setLoadSaveChangesProcess] = useState(false);
  const [badgeCommentLike, setBadgeCommentLike] = useState("");
  const [deleteCommentInfo, setDeleteCommentInfo] = useState(null);
  const [allBadges, setAllBadges] = useState(
    !props?.location?.state?.allBadges
      ? true
      : props?.location?.state?.allBadges
  );
  const [myBadges, setMyBadges] = useState(
    !props?.location?.state?.myBadges ? false : props?.location?.state?.myBadges
  );
  var [badgeImage, setBadgeImage] = useState(null);
  const [userListing, setUserListing] = useState([]);
  const [badgeImageID, setBadgeImageID] = useState(null);
  const [badgeImageName, setBadgeImageName] = useState(null);
  const [badgeProcess, setBadgeProcess] = useState(false);
  const [isPublic, setisPublic] = useState(true);
  const [imageSelectedCheck, setImageSelectedCheck] = useState(false);
  /*   const [load, setLoad] = useState(5);
   */
  const [ValidEditComment, setValidEditComment] = useState(false);
  const [likeProcess, setLikeProcess] = useState(false);
  const [commentProcess, setCommentProcess] = useState(false);
  const [commentLikeProcess, setCommentLikeProcess] = useState(false);
  const [commentUpdateProcess, setCommentUpdateProcess] = useState(false);
  const [commentLikeInfo, setCommentLikeInfo] = useState([]);

  //Modals
  const [ModalToggle, setModalToggle] = useState(false);
  const [NextModalToggle, setNextModalToggle] = useState(false);
  const [PreviewModalToggle, setPreviewModalToggle] = useState(false);
  const [badgeLikeModal, setBadgeLikeModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [editCommentModal, setEditCommentModal] = useState(false);
  const [commentLikeModal, setCommentLikeModal] = useState(false);
  const [BlogModulePermissions, setBlogModulePermissions] = useState([]);
  const [BadgeModulePermissions, setBadgeModulePermissions] = useState(null);
  const [AnnouncementModulePermissions, setAnnouncementModulePermissions] =
    useState([]);
  //Cognito User
  var userSelector = useSelector((state) => state.UserReducer).user;
  var companySelector = useSelector((state) => state.CompanyReducer).company;
  var cognitoUser = useSelector((state) => state.UserReducer).cognitoUser;
  const ModulePermissionSelector = useSelector(
    (state) => state.UserReducer
  ).ModuleWisePermission;
  const userRoleSelector = useSelector((state) => state.UserReducer).userRole;
  const history = useHistory();

  const [AddComment, setAddComment] = useState(false);
  const [AddCommentSubmitted, setAddCommentSubmitted] = useState(false);
  const [AddCommentBlog, setAddCommentBlog] = useState(null);
  const [CommentEdited, setCommentEdited] = useState(false);

  const [newsToShow, setNewsToShow] = useState(4);

  const EmptyIcon = require("@src/assets/images/icons/empty.svg").default;

  const newsShow = () => {
    setNewsToShow(newsToShow + 2);
  };

  const newsShowSub = () => {
    setNewsToShow(4);
  };

  //read more function
  const readmore = (i) => {
    setIndex1(i);
    setRead(!read);
  };

  //Toast Component
  const ToastComponent = ({ title, color }) => (
    <Fragment>
      <div className="toastify-header pb-0">
        <div className="title-wrapper">
          <Avatar size="sm" color={color} icon={<X size={12} />} />
          <h6 className="toast-title">{title}</h6>
        </div>
      </div>
    </Fragment>
  );

  const ToastComponentSuccess = ({ title, color }) => (
    <Fragment>
      <div className="toastify-header pb-0">
        <div className="title-wrapper">
          <Avatar size="sm" color={color} icon={<Check size={12} />} />
          <h6 className="toast-title">{title}</h6>
        </div>
      </div>
    </Fragment>
  );

  //Used to dispatch actions and trigger state changes to the store
  const dispatch = useDispatch();

  //select check box function on modal
  const selectsCheckBox = () => {
    setisPublic(!isPublic);
  };

  //select badge image check box function
  const selectImageCheckBox = () => {
    setImageSelectedCheck(imageSelectedCheck);
  };

  //Function for badge image
  const handleBadgeImageChange = (
    selectedbadgeImageID,
    selectedBadgeImage,
    selectedBadgeImageName
  ) => {
    setBadgeImageID(selectedbadgeImageID);
    setBadgeImage(selectedBadgeImage);
    setBadgeImageName(selectedBadgeImageName);
  };

  //function for creating badge
  const handleCreateBadge = async (e) => {
    // e.preventDefault();
    setBadgeProcess(true);
    let payload = {
      BadgeImage: badgeImageID,
      sendingBadgeUser: sendingBadgeUser,
      description: description,
      userID: userID,
      companyID: companyID,
      isPublic: isPublic,
      badgeUrl: badgeImage,
      badgeImageName: badgeImageName,
    };
    // console.log("badge payload", payload);
    try {
      await dispatch(
        createBadgeAction(payload, userSelector, companySelector, cognitoUser)
      );
      toast.success(
        <ToastComponentSuccess title="Recognition Send!" color="success" />,
        {
          autoClose: 3000,
          hideProgressBar: true,
          closeButton: false,
        }
      );
    } catch (error) {
      toast.error(<ToastComponent title={error.message} color="danger" />, {
        autoClose: 3000,
        hideProgressBar: true,
        closeButton: false,
      });
      console.log("Error in sending Recognition!", error);
    }
    try {
      await dispatch(getCompany());
    } catch (err) {
      console.log(err);
    }
    setSendingBadgeUser([]);
    setDescription(null);
    setBadgeImage(null);
    setBadgeProcess(false);
    setPreviewModalToggle(!PreviewModalToggle);
  };

  //function for nested comment
  const handleCommentReply = async (e, badgeID, parentID) => {
    e.preventDefault();
    let comment = commentReply;
    let newComment = comment.replace(/(\r\n|\n|\r)/gm, "").trim();
    // console.log("comment", comment, newComment);
    if (newComment != "") {
      var payload = {
        userID: badgeOwnerID,
        badgeID: badgeID,
        content: commentReply,
        parentID: parentID,
        createdAt: new Date().toISOString(),
      };
      // console.log("BADGE COMMENT REPLYYYYYYYY PAYLOAD", payload);
      try {
        await dispatch(addBadgesCommentAction(payload, cognitoUser));
      } catch (err) {
        console.log(err);
      }
      setcommentReply("");
    } else {
      setCommentError(true);
    }
  };

  //function for comment
  const handleAddBadgeComment = async (e, badgeID) => {
    e.preventDefault();
    setCommentProcess(true);
    let comment = commentContent;
    let newComment = comment.replace(/(\r\n|\n|\r)/gm, "").trim();
    // console.log("comment", comment, newComment);
    if (newComment != "") {
      var payload = {
        userID: badgeOwnerID,
        badgeID: badgeID,
        content: commentContent,
        createdAt: new Date().toISOString(),
      };
      // console.log("BADGE COMMENT PAYLOAD", payload);
      try {
        await dispatch(addBadgesCommentAction(payload));
      } catch (err) {
        console.log(err);
      }
      setAddCommentBlog(null);
      setAddComment(false);
      setAddCommentSubmitted(false);
      setcommentContent("");
      await setAddCommentBlog(null);
    } else {
      setCommentError(true);
      setAddCommentBlog(null);
      setAddComment(false);
      setAddCommentSubmitted(false);
      setcommentContent("");
      await setAddCommentBlog(null);
    }
    setCommentProcess(false);
  };

  //function for like badge
  const handleBadgeLike = async (e, badgeID, likes) => {
    e.preventDefault();
    setLikeProcess(true);
    var likedarr = likes.filter((item) => {
      return userID === item.userID;
    });
    if (likedarr.length > 0) {
      return console.log("Post already likedd by you");
    }
    var payload = {
      numberLikes: numberLikes,
      userID: userID,
      badgeID: badgeID,
      companyID: companyID,
      likeBadgeBadgeId: badgeID,
    };

    // console.log("badgelikepayload", payload);
    try {
      await dispatch(likeBadgesAction(payload, cognitoUser));
    } catch (err) {
      console.log(err);
    }
    setLikeProcess(false);
  };

  //function to delete comment
  const handleDeleteComment = async (e, commentID) => {
    e.preventDefault();
    let payload = {
      id: commentID,
    };

    try {
      await dispatch(deleteBadgeCommentAction(payload, cognitoUser));
    } catch (err) {
      console.log(err);
    }
  };

  //function to delete badge like
  const handleDeleteBadgeLike = async (e, items) => {
    e.preventDefault();
    setLikeProcess(true);
    var likeIDItem = items.filter((item) => {
      return item.likeUser.id === userID;
    });
    let payload = {
      id: likeIDItem[0].id,
    };
    try {
      await dispatch(deleteBadgeLikeAction(payload, cognitoUser));
    } catch (err) {
      console.log(err);
    }
    setLikeProcess(false);
  };

  //function to delete comment like
  const handleDeleteBadgeCommentLike = async (e, items) => {
    e.preventDefault();
    setCommentLikeProcess(true);
    var commentLikeIDItem = items.filter((item) => {
      return item.likeUser.id === userID;
    });
    let payload = {
      id: commentLikeIDItem[0].id,
    };
    try {
      await dispatch(deleteBadgeCommentLikeAction(payload, cognitoUser));
    } catch (err) {
      console.log(err);
    }
    setCommentLikeProcess(false);
  };

  //function to edit or update comment
  const handleEditComment = async (commentItem) => {
    // console.log("update comment", commentItem);
    setCommentUpdateProcess(true);
    if (CommentEdited == true) {
      try {
        let comment = commentReplyContent;
        let newComment = comment.replace(/(\r\n|\n|\r)/gm, "").trim();
        // console.log("comment", newComment);
        if (newComment != "") {
          let payload = {
            id: commentItem.id,
            userID: userID,
            content: commentReplyContent,
            badgeID: commentItem.badgeID,
            createdAt: new Date().toISOString(),
            parentID: commentItem.id,
          };
          // console.log(" update comment payload", payload);
          try {
            await dispatch(updateBadgeCommentAction(payload, cognitoUser));
          } catch (err) {
            console.log(err);
          }
          await dispatch(getCompany());
        }
      } catch (error) {
        console.log("error in editing comment", error);
      }
      setCommentEdited(false);
      setEditCommentMode(false);
      setEditCommentData(null);
      setValidEditComment(false);
    } else {
      setCommentEdited(false);
      setEditCommentMode(false);
      setEditCommentData(null);
      setValidEditComment(false);
    }
    setCommentUpdateProcess(false);
  };

  //function to like comment
  const handleBadgeCommentLike = async (e, commentID, likes) => {
    setCommentLikeProcess(true);
    e.preventDefault();
    var likedarray = likes.filter((item) => {
      return userID === item.userID;
    });
    if (likedarray.length > 0) {
      return console.log("Post already likedd by you");
    }
    var payload = {
      numberLikes: badgeCommentLike,
      userID: userID,
      commentID: commentID,
    };
    // console.log("NESTEDCOMMENTLIKE", payload);
    try {
      await dispatch(badgeCommentLikeAction(payload, cognitoUser));
    } catch (err) {
      console.log(err);
    }
    setCommentLikeProcess(false);
  };

  //React hook(Using this you tell Reat that your component needs to do something after render)
  useEffect(() => {
    setLoadProcess(true);
    setLoadSaveChangesProcess(true);
    getData();
  }, [companySelector]);

  //function called in useEffect
  const getData = async () => {
    let userArr = [];
    var logInUserId = cognitoUser.userName;
    var userProfileImage = userSelector.profileImage;
    if (
      Object.keys(companySelector).length === 0 ||
      Object.keys(userSelector).length === 0
    ) {
      await dispatch(getCompany());
      await dispatch(getUser());
    } else {
      let usr = await Auth.currentUserInfo();
      // console.log("userID in badge", usr);
      var usersOfCompany = companySelector.users.items.filter((item) => {
        return item.id !== logInUserId;
      });
      usersOfCompany.map((items) => {
        items.name = `${items.first_name} ${items.last_name}`;
        userArr.push(items);
      });
      let blogPermissions = ModulePermissionSelector.filter(
        (option) => option.module == "Blog Post"
      );
      setBlogModulePermissions(blogPermissions[0]);
      // console.log("blogPermissions", blogPermissions);
      let badgePermissions = ModulePermissionSelector.filter(
        (option) => option.module == "Recognition"
      );
      setBadgeModulePermissions(badgePermissions[0]);
      // console.log("badgePermissions", badgePermissions);
      let announcementPermissions = ModulePermissionSelector.filter(
        (option) => option.module == "Announcement"
      );
      setAnnouncementModulePermissions(announcementPermissions[0]);
      // console.log("announcementPermissions", announcementPermissions);
      let users = userArr.filter((option) => option.id !== usr.username);
      // console.log("users in badge", users);
      setUserListing(users);
    }
    // console.log("USEEFFECT CALLED");
    try {
      await dispatch(listBadgeImagesAction());
    } catch (err) {
      console.log(err);
    }

    try {
      await dispatch(listBadgeAction(cognitoUser));
    } catch (err) {
      console.log(err);
    }
    try {
      await dispatch(listAllBadgesAction(cognitoUser));
    } catch (err) {
      console.log(err);
    }
    /* try {
      var userInfo = await API.graphql(
        graphqlOperation(getUser, { id: logInUserId })
      );
      console.log("UserINFO", userInfo);
    } catch (err) {
      console.log(err);
    } */

    setUserProfileImage(userProfileImage);

    try {
      await Auth.currentUserInfo().then((user) => {
        if (user !== null) {
          // console.log("user: ", user);
          setUserID(user.attributes.sub);

          setCompanyID(user.attributes["custom:companyID"]);

          setUserName(
            user.attributes["custom:first_name"] +
            " " +
            user.attributes["custom:last_name"]
          );
          setBadgeOwnerID(user.attributes.sub);
          setnumberLikes(1);
          setBadgeCommentLike(1);
          setUserRole(user.attributes["custom:role"]);
        } else if (user === null) {
          setUserID("");
        }
      });
    } catch (err) { }
    setLoadProcess(false);
    setLoadSaveChangesProcess(false);
  };

  //returns the part of the state that we want
  var badgeimagearr = useSelector((state) => state.badgeReducer).badgeImageArr;
  // console.log("BadgeImageArrrrrr", badgeimagearr);
  var allbadgearr = useSelector((state) => state.badgeReducer).allBadgeArr;
  //console.log("All Badge Arrrrrr", allbadgearr);

  if (!props.empID) {
    allbadgearr = allbadgearr;
  } else {
    var newAllBadgeArr = [];
    for (let i = 0; i < allbadgearr?.length; i++) {
      for (let j = 0; j < allbadgearr[i]?.BadgeSending?.items?.length; j++) {
        if (allbadgearr[i]?.BadgeSending?.items[j]?.userID === props.empID) {
          newAllBadgeArr.push(allbadgearr[i]);
        }
      }
    }
    allbadgearr = newAllBadgeArr;
    //console.log("Cond ALL BADGES",allbadgearr)
  }
  var myBadgeArr = useSelector((state) => state.badgeReducer).badgearr;
  //console.log("MY Badge Arrrrrr", myBadgeArr);

  return (
    /* <!-- BEGIN: Content--> */

    <Fragment>
      {BadgeModulePermissions !== null ? (
        BadgeModulePermissions?.ViewBadge == true ||
          userRoleSelector == "SuperAdmin" ? (
          <div>
            <div className="header-navbar-shadow"></div>
            <div className="content-wrapper">
              <div className="content-header row">
                {!props.empID ? (
                  <div className="content-header-left col-md-9 col-12 mb-2">
                    <div className="row breadcrumbs-top">
                      {myBadges === true ? (
                        <div className="col-12">
                          <h2 className="content-header-title float-left mb-0 border-0">
                            My Recognitions
                          </h2>
                        </div>
                      ) : (
                        <div className="col-12">
                          <h2 className="content-header-title float-left mb-0 border-0">
                            Recognition Wall
                          </h2>
                        </div>
                      )}
                    </div>
                  </div>
                ) : (
                  <></>
                )}
                {!props.empID ? (
                  <div className="content-header-right text-md-right col-md-3 col-12 d-md-block">
                    {BadgeModulePermissions?.CreateBadge == true ||
                      userRoleSelector == "SuperAdmin" ? (
                      <div className="form-group breadcrumb-right">
                        <a
                          className="btn btn-primary btn-sm mb-1 waves-effect waves-float waves-light"
                          data-toggle="modal"
                          data-target="#badges-model"
                          onClick={() => setModalToggle(!ModalToggle)}
                        >
                          Send Recognition
                        </a>
                      </div>
                    ) : null}

                    <Modal
                      size="lg"
                      isOpen={ModalToggle}
                      toggle={(e) => {
                        setBadgeImage(null);
                        setModalToggle(!ModalToggle);
                      }}
                    >
                      <ModalHeader
                        toggle={(e) => {
                          setBadgeImage(null);
                          setModalToggle(!ModalToggle);
                        }}
                      >
                        Send Recognition
                      </ModalHeader>

                      <ModalBody>
                        <h5>Select Badge</h5>

                        <div className="badges-box-wrp">
                          {badgeimagearr.length > 0 ? (
                            badgeimagearr.map((item, index) =>
                              item.BadgeImage === badgeImage ? (
                                <div
                                  className="badge-box selected"
                                  key={index}
                                  onClick={() => {
                                    handleBadgeImageChange(
                                      item.id,
                                      item.BadgeImage,
                                      item.BadgeName
                                    );
                                  }}
                                  style={{ cursor: "pointer" }}
                                >
                                  <span className="check">
                                    <input type="checkbox" checked />
                                  </span>

                                  <div className="badge-center">
                                    <img
                                      src={item.BadgeImage}
                                      alt=""
                                      onChange={selectImageCheckBox}
                                    />
                                    <h5 className="font-weight-bolder">
                                      {item.BadgeName}
                                    </h5>
                                  </div>
                                </div>
                              ) : (
                                <div
                                  className="badge-box"
                                  onClick={() => {
                                    handleBadgeImageChange(
                                      item.id,
                                      item.BadgeImage,
                                      item.BadgeName
                                    );
                                  }}
                                  style={{ cursor: "pointer" }}
                                >
                                  <div className="badge-center">
                                    <img
                                      src={item.BadgeImage}
                                      alt=""
                                      onChange={selectImageCheckBox}
                                    />
                                    <h5 className="font-weight-bolder">
                                      {item.BadgeName}
                                    </h5>
                                  </div>
                                </div>
                              )
                            )
                          ) : (
                            <div>No Recognitions</div>
                          )}
                          <div
                            className="badge-box"
                            style={{ cursor: "pointer", visibility: "hidden" }}
                          >
                            <div className="badge-center">
                              <h5 className="font-weight-bolder"></h5>
                            </div>
                          </div>
                        </div>
                      </ModalBody>

                      <ModalFooter>
                        <button
                          type="button"
                          className="btn btn-outline-primary"
                          data-dismiss="modal"
                          onClick={() => {
                            setModalToggle(!ModalToggle);
                            setBadgeImage(null);
                          }}
                        >
                          Cancel
                        </button>
                        {badgeImage === null ? (
                          <button
                            type="button"
                            className="disabled btn btn-primary"
                            disabled
                          >
                            Next
                          </button>
                        ) : (
                          <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => {
                              setNextModalToggle(!NextModalToggle);
                              setModalToggle(!ModalToggle);
                            }}
                          >
                            Next
                          </button>
                        )}
                      </ModalFooter>
                    </Modal>

                    {/* Next Modal */}

                    <Modal
                      size="lg"
                      isOpen={NextModalToggle}
                      toggle={(e) => {
                        setDescription(null);
                        setNextModalToggle(!NextModalToggle);
                      }}
                    >
                      <ModalHeader
                        toggle={(e) => {
                          setDescription(null);
                          setNextModalToggle(!NextModalToggle);
                        }}
                      >
                        Send Recognition
                      </ModalHeader>

                      <ModalBody>
                        <div className="d-flex justify-content-start align-items-center mb-1">
                          {/* <!-- avatar --> */}
                          <div className="avatar mr-1">
                            {userProfileImage === null ? (
                              <img src={userIcon} height="50" width="50" />
                            ) : (
                              <>
                                <AmplifyS3Image
                                  className="badge-profile-img"
                                  imgKey={userProfileImage}
                                // theme={{
                                //   photoImg: { width: "42px", height: "42px" },
                                // }}
                                />
                              </>
                            )}
                          </div>
                          {/* <!--/ avatar --> */}
                          <div className="profile-user-info">
                            <h6 className="mb-0">
                              <span
                                onClick={() =>
                                  history.push("/employee-profile/" + userID)
                                }
                              >
                                {userName}
                              </span>
                            </h6>
                            <small className="text-muted">
                              Send Recognition
                            </small>
                          </div>
                        </div>

                        <div className="row">
                          <div className="col-sm-6">
                            <div className="bage-small-preview">
                              <div className="badge-box">
                                <div className="badge-center">
                                  <img src={badgeImage} />
                                  <h5 className="font-weight-bolder">
                                    {badgeImageName}
                                  </h5>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-sm-6">
                            <div className="form-group">
                              <label for="credit-card">To</label>
                              <Multiselect
                                placeholder="Search"
                                // invalid={sendingBadgeUser.length == 0}
                                // selectedValues={roles}
                                options={userListing}
                                onRemove={(
                                  selectedList,
                                  selectedItem,
                                  index
                                ) => {
                                  setSendingBadgeUser(selectedList);
                                }}
                                onSelect={(selectedList, selectedItem, index) =>
                                  setSendingBadgeUser(selectedList)
                                } // Function will trigger on select event
                                displayValue="name" // Property name to display in the dropdown options
                                required
                              />
                            </div>

                            <div className="form-group">
                              <label for="exampleFormControlTextarea1">
                                Note
                              </label>
                              <textarea
                                className="form-control"
                                id="exampleFormControlTextarea1"
                                rows="3"
                                placeholder="Write your message here!"
                                onChange={(e) => {
                                  setDescription(e.target.value);
                                }}
                              ></textarea>
                            </div>
                          </div>
                        </div>
                      </ModalBody>

                      <ModalFooter>
                        <button
                          type="button"
                          className="btn btn-outline-primary"
                          data-dismiss="modal"
                          onClick={() => {
                            setNextModalToggle(!NextModalToggle);
                            setModalToggle(!ModalToggle);
                          }}
                        >
                          Back
                        </button>
                        {description === null ||
                          description.replace(/(\r\n|\n|\r)/gm, "").trim() ===
                          "" ||
                          sendingBadgeUser.length === 0 ? (
                          <button
                            type="button"
                            className="disabled btn btn-primary"
                            disabled
                          >
                            Preview
                          </button>
                        ) : (
                          <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => {
                              if (sendingBadgeUser.length !== 0) {
                                setPreviewModalToggle(!PreviewModalToggle);
                                setNextModalToggle(!NextModalToggle);
                              } else {
                                // console.log(
                                //   "sendingBadgeUser",
                                //   sendingBadgeUser.length
                                // );
                              }
                            }}
                          >
                            Preview
                          </button>
                        )}
                      </ModalFooter>
                    </Modal>

                    {/* Preview modal */}
                    <Modal
                      size="lg"
                      isOpen={PreviewModalToggle}
                      toggle={(e) => {
                        setPreviewModalToggle(!PreviewModalToggle);
                      }}
                    >
                      <ModalHeader
                        toggle={(e) => {
                          setPreviewModalToggle(!PreviewModalToggle);
                        }}
                      >
                        Preview Recognition
                      </ModalHeader>

                      <ModalBody>
                        <div className="d-flex justify-content-start align-items-center mb-1">
                          {/* <!-- avatar --> */}
                          <div className="avatar mr-1">
                            {userProfileImage === null ? (
                              <img src={userIcon} height="50" width="50" />
                            ) : (
                              <>
                                <AmplifyS3Image
                                  className="badge-profile-img"
                                  imgKey={userProfileImage}
                                // theme={{
                                //   photoImg: { width: "42px", height: "42px" },
                                // }}
                                />
                              </>
                            )}
                          </div>
                          {/* <!--/ avatar --> */}
                          <div className="profile-user-info">
                            <h6 className="mb-0">{userName}</h6>
                            <small className="text-muted">
                              Send Recognition
                            </small>
                          </div>
                        </div>

                        <div className="border p-2 mb-1 r">
                          <div className="text-center mb-2">
                            <small>{userName} sends recognition to</small>
                            <p className="">
                              {sendingBadgeUser.map((items, index) =>
                                index === sendingBadgeUser.length - 2
                                  ? items.name + " and "
                                  : index === sendingBadgeUser.length - 1
                                    ? items.name + ". "
                                    : items.name + ", "
                              )}
                            </p>

                            <div
                              className="badge-previw"
                              style={{
                                backgroundImage: `URL(${backgroundImage})`,
                                backgroundRepeat: "no-repeat",
                                backgroundPosition: "center",
                              }}
                            >
                              <div className="badge-box">
                                <img src={badgeImage} alt="" />

                                <h5 className="font-weight-bolder mt-1">
                                  {badgeImageName}
                                </h5>
                              </div>
                            </div>
                          </div>

                          <p className="text-center">{description}</p>
                        </div>

                        {isPublic === true ? (
                          <div className="custom-control custom-checkbox mb-1">
                            <input
                              className="custom-control-input"
                              id="remember-me"
                              tabindex="3"
                              type="checkbox"
                              checked
                              onChange={selectsCheckBox}
                            />
                            <label
                              className="custom-control-label"
                              for="remember-me"
                            >
                              Do you want to share this on Recognition Wall?
                            </label>
                          </div>
                        ) : (
                          <div className="custom-control custom-checkbox mb-1">
                            <input
                              className="custom-control-input"
                              tabindex="3"
                              id="remember-me"
                              type="checkbox"
                              onChange={selectsCheckBox}
                            />
                            <label
                              className="custom-control-label"
                              for="remember-me"
                            >
                              Do you want to share this on Recognition Wall?
                            </label>
                          </div>
                        )}
                      </ModalBody>

                      <ModalFooter>
                        <button
                          type="button"
                          className="btn btn-outline-primary"
                          data-dismiss="modal"
                          onClick={() => {
                            setPreviewModalToggle(!PreviewModalToggle);
                            setNextModalToggle(!NextModalToggle);
                            setSendingBadgeUser([]);
                            setDescription(null);
                          }}
                        >
                          Back
                        </button>
                        {badgeProcess === true ? (
                          <button
                            type="button"
                            className="disabled btn btn-primary"
                            disabled
                            data-dismiss="modal"
                          >
                            <Spinner color="light" size="sm" />
                            Send Card
                          </button>
                        ) : (
                          <button
                            type="button"
                            className="btn btn-primary"
                            data-dismiss="modal"
                            onClick={(e) => {
                              // console.log(sendingBadgeUser);
                              handleCreateBadge(e);
                            }}
                          >
                            Send Card
                          </button>
                        )}
                      </ModalFooter>
                    </Modal>
                  </div>
                ) : (
                  <></>
                )}
              </div>
              <div className="content-body">
                <section>
                  <div className="row">
                    {/*  <!-- left profile info section --> */}
                    {!props.empID ? (
                      <div className="col-lg-3 col-12 order-2 order-lg-1">
                        {/* <!-- twitter feed card --> */}
                        <div className="card">
                          <div className="card-body">
                            <div className="d-flex justify-content-between align-items-start">
                              <div className="d-flex justify-content-start align-items-center mb-1">
                                {/* <!-- avatar --> */}
                                <div className="avatar mr-1">
                                  {userProfileImage === null ? (
                                    <img
                                      src={userIcon}
                                      height="40"
                                      width="40"
                                    />
                                  ) : (
                                    <>
                                      <AmplifyS3Image
                                        className="badge-profile-img"
                                        imgKey={userProfileImage}
                                      // theme={{
                                      //   photoImg: { width: "40px", height: "40px" },
                                      // }}
                                      />
                                    </>
                                  )}
                                </div>
                                {/* <!--/ avatar --> */}
                                <div className="profile-user-info">
                                  <h6 className="mb-0">
                                    <a
                                      onClick={() =>
                                        history.push(
                                          "/employee-profile/" + userID
                                        )
                                      }
                                    >
                                      {userName}
                                    </a>
                                  </h6>
                                  <small className="text-muted">
                                    {userRole}
                                  </small>
                                </div>
                              </div>
                              <div className="dropdown chart-dropdown">
                                <UncontrolledDropdown>
                                  <DropdownToggle className="pr-1" tag="span">
                                    <button
                                      type="button"
                                      className="btn btn-icon btn-sm btn-outline-primary"
                                    >
                                      <Filter size={15}></Filter>
                                    </button>
                                  </DropdownToggle>
                                  <DropdownMenu right>
                                    <DropdownItem>
                                      <span
                                        className="align-middle ml-50 cursor-pointe"
                                        onClick={() => {
                                          setMyBadges(false);
                                          setAllBadges(true);
                                        }}
                                      >
                                        All Recognitions
                                      </span>
                                    </DropdownItem>
                                    <DropdownItem className="w-100">
                                      <span
                                        className="align-middle ml-50 cursor-pointe"
                                        onClick={() => {
                                          setMyBadges(true);
                                          setAllBadges(false);
                                        }}
                                      >
                                        My Recognitions
                                      </span>
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                              </div>
                            </div>

                            <hr />

                            <>
                              <h5>My Recognitions</h5>
                              <div className="my-badges-short d-flex flex-wrap ">
                                {myBadgeArr.length > 0 ? (
                                  myBadgeArr
                                    .slice(0, newsToShow)
                                    .map((item) => (
                                      <div className="box">
                                        <span className="badge-icon avatar bg-light-primary">
                                          <img
                                            src={item.BadgeImage.BadgeImage}
                                            alt=""
                                          />
                                        </span>
                                        <h6 className="font-weight-bolder mt-1">
                                          {item.BadgeImage.BadgeName}
                                        </h6>
                                        <small>
                                          <a
                                            onClick={() =>
                                              history.push(
                                                "/employee-profile/" +
                                                item.createdByUser.id
                                              )
                                            }
                                          >
                                            By {item.createdByUser.first_name}{" "}
                                            {item.createdByUser.last_name}
                                          </a>
                                        </small>
                                      </div>
                                    ))
                                ) : (
                                  <></>
                                )}
                              </div>
                              <hr />
                              {myBadgeArr !== null && myBadgeArr.length > 1 ? (
                                newsToShow > myBadgeArr.length &&
                                  myBadgeArr.length > 4 ? (
                                  <div className="text-center mt-2">
                                    <a
                                      href="javascript:;"
                                      onClick={newsShowSub}
                                    >
                                      <small>View Less </small>
                                    </a>
                                  </div>
                                ) : newsToShow < myBadgeArr.length ? (
                                  <div className="text-center mt-2">
                                    <a href="javascript:;" onClick={newsShow}>
                                      <small>View More </small>
                                    </a>
                                  </div>
                                ) : null
                              ) : (
                                <></>
                              )}
                            </>
                          </div>
                        </div>
                        {/*               <!--/ twitter feed card -->
                         */}{" "}
                      </div>
                    ) : (
                      <></>
                    )}
                    {/* <!--/ left profile info section --> */}

                    {/* 
      
                  <!-- center profile info section --> */}
                    <div
                      className={
                        !props.empID
                          ? "col-lg-6 col-12 order-1 order-lg-2"
                          : "col-lg-12 col-12 order-1 order-lg-2"
                      }
                    >
                      {/* <!-- post 1 --> */}
                      <Modal
                        isOpen={badgeLikeModal}
                        toggle={() => setBadgeLikeModal(!badgeLikeModal)}
                      >
                        <ModalHeader
                          toggle={() => setBadgeLikeModal(!badgeLikeModal)}
                        >
                          Likes
                        </ModalHeader>

                        <ModalBody>
                          {likeinfo.length > 0 ? (
                            likeinfo.map((item, index) => (
                              <div
                                className="d-flex justify-content-start align-items-center mb-1"
                                key={index}
                              >
                                <div className="avatar mt-25 mr-75 likeawsImg">
                                  {item.likeUser.profileImage === null ? (
                                    <img
                                      src={userIcon}
                                      height="40"
                                      width="40"
                                    />
                                  ) : (
                                    <>
                                      <AmplifyS3Image
                                        className="badge-profile-img"
                                        imgKey={item.likeUser.profileImage}
                                        theme={{
                                          photoImg: {
                                            width: "40px",
                                            height: "40px",
                                          },
                                        }}
                                      />
                                    </>
                                  )}
                                  {/* <img src={image18} alt="Avatar" height="34"
                                                    width="34" /> */}
                                </div>
                                <div className="profile-user-info">
                                  <h6 className="mb-0">
                                    {" "}
                                    <a
                                      onClick={() =>
                                        history.push(
                                          "/employee-profile/" +
                                          item.likeUser.id
                                        )
                                      }
                                    >
                                      {item.likeUser.first_name}{" "}
                                      {item.likeUser.last_name}{" "}
                                    </a>
                                  </h6>
                                  {/* <small className="text-muted">{}</small> */}
                                </div>
                              </div>
                            ))
                          ) : (
                            <></>
                          )}
                        </ModalBody>
                        <ModalFooter>
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() => setBadgeLikeModal(!badgeLikeModal)}
                          >
                            Close
                          </button>
                        </ModalFooter>
                      </Modal>

                      <Modal
                        isOpen={deleteModal}
                        toggle={() => setDeleteModal(!deleteModal)}
                        className="modal-dialog-centered"
                      >
                        <ModalHeader
                          toggle={() => setDeleteModal(!deleteModal)}
                        >
                          Delete comment?
                        </ModalHeader>
                        <ModalBody>
                          <h5>
                            Are you sure you want to permanently remove this
                            comment?
                          </h5>
                        </ModalBody>
                        <ModalFooter>
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={(e) => {
                              handleDeleteComment(e, deleteCommentInfo.id);
                              setDeleteModal(!deleteModal);
                            }}
                          >
                            Delete
                          </button>
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() => setDeleteModal(!deleteModal)}
                          >
                            Cancel
                          </button>
                        </ModalFooter>
                      </Modal>

                      <Modal
                        isOpen={commentLikeModal}
                        toggle={() => setCommentLikeModal(!commentLikeModal)}
                      >
                        <ModalHeader
                          toggle={() => setCommentLikeModal(!commentLikeModal)}
                        >
                          Likes
                        </ModalHeader>

                        <ModalBody>
                          {commentLikeInfo.length > 0 ? (
                            commentLikeInfo.map((item, index) => (
                              <div
                                className="d-flex justify-content-start align-items-center mb-1"
                                key={index}
                              >
                                <div className="avatar mt-25 mr-75 likeawsImg">
                                  {item.likeUser.profileImage === null ? (
                                    <img
                                      src={userIcon}
                                      height="40"
                                      width="40"
                                    />
                                  ) : (
                                    <>
                                      <AmplifyS3Image
                                        className="badge-profile-img"
                                        imgKey={item.likeUser.profileImage}
                                        theme={{
                                          photoImg: {
                                            width: "40px",
                                            height: "40px",
                                          },
                                        }}
                                      />
                                    </>
                                  )}
                                  {/* <img src={image18} alt="Avatar" height="34"
                                                    width="34" /> */}
                                </div>
                                <div className="profile-user-info">
                                  <h6 className="mb-0">
                                    {" "}
                                    <a
                                      onClick={() =>
                                        history.push(
                                          "/employee-profile/" +
                                          item.likeUser.id
                                        )
                                      }
                                    >
                                      {item.likeUser.first_name}{" "}
                                      {item.likeUser.last_name}{" "}
                                    </a>
                                  </h6>
                                  {/* <small className="text-muted">{}</small> */}
                                </div>
                              </div>
                            ))
                          ) : (
                            <></>
                          )}
                        </ModalBody>
                        <ModalFooter>
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              setCommentLikeModal(!commentLikeModal)
                            }
                          >
                            Close
                          </button>
                        </ModalFooter>
                      </Modal>

                      {allbadgearr.length > 0 ? (
                        allBadges === true && myBadges === false ? (
                          allbadgearr.map((item, i) => (
                            <div className="card profileimagetest" key={i}>
                              <div className="card-body">
                                <div className="d-flex justify-content-between align-items-start">
                                  <div className="d-flex justify-content-start align-items-center mb-1">
                                    {/* <!-- avatar --> */}
                                    <div
                                      className="avatar mr-1"
                                      key={i + item.createdByUser.id}
                                    >
                                      {item.createdByUser.profileImage ===
                                        null ? (
                                        <img
                                          src={userIcon}
                                          height="50"
                                          width="50"
                                        />
                                      ) : (
                                        <>
                                          <AmplifyS3Image
                                            className="badge-profile-img"
                                            imgKey={
                                              item.createdByUser.profileImage
                                            }
                                          />
                                        </>
                                      )}
                                    </div>
                                    {/* <!--/ avatar --> */}
                                    <div className="profile-user-info">
                                      <h6 className="mb-0">
                                        <a
                                          onClick={() =>
                                            history.push(
                                              "/employee-profile/" +
                                              item.createdByUser.id
                                            )
                                          }
                                        >
                                          {item.createdByUser.first_name}{" "}
                                          {item.createdByUser.last_name}
                                        </a>
                                      </h6>
                                      <small className="card-text">
                                        Send Recognition -{" "}
                                        {moment(item.createdAt).fromNow()}
                                      </small>
                                    </div>
                                  </div>
                                  {/*<button type="button" className="btn btn-outline-primary btn-sm round waves-effect">Publish</button>
                                   */}
                                </div>

                                {!read ? (
                                  item.description.length > 100 ? (
                                    <p className="card-text">
                                      {item.description.slice(0, 100)} . . . .{" "}
                                    </p>
                                  ) : (
                                    <p className="card-text">
                                      {item.description}
                                    </p>
                                  )
                                ) : (
                                  <p className="card-text" id={`project${i}`}>
                                    {item.description}
                                  </p>
                                )}
                                {item.description.length > 100 ? (
                                  <div className="w-100 text-right mb-1">
                                    <a
                                      href="#"
                                      data-toggle="collapse"
                                      href={`#project${i}`}
                                      role="button"
                                      onClick={() => readmore(i)}
                                      aria-expanded="false"
                                      aria-controls="collapseExample"
                                      className="w-100 text-right mb-1"
                                    >
                                      {read && i === index1
                                        ? "Read Less"
                                        : "Read More"}
                                    </a>
                                  </div>
                                ) : null}

                                {/* <div className="w-100 text-right mb-1">
                            <a href="#">...see more</a>
                          </div> */}
                                {/* <!-- post img --> */}
                                <div className="d-flex flex-wrap badge-users">
                                  {item.BadgeSending.items.length > 0 ? (
                                    item.BadgeSending.items.map(
                                      (badgeSendingitems, i) => (
                                        <div
                                          className="px-1 b-users text-center mb-2"
                                          key={i}
                                        >
                                          <div
                                            className="avatar avatar-lg"
                                            key={i + badgeSendingitems.user.id}
                                          >
                                            {badgeSendingitems.user
                                              .profileImage === null ? (
                                              <img
                                                src={userIcon}
                                                height="50"
                                                width="50"
                                              />
                                            ) : (
                                              <>
                                                <AmplifyS3Image
                                                  imgKey={
                                                    badgeSendingitems.user
                                                      .profileImage
                                                  }
                                                  className="badge-profile-img"
                                                />
                                              </>
                                            )}
                                          </div>
                                          <small className="d-block">
                                            <a
                                              onClick={() =>
                                                history.push(
                                                  "/employee-profile/" +
                                                  badgeSendingitems.user.id
                                                )
                                              }
                                            >
                                              {
                                                badgeSendingitems.user
                                                  .first_name
                                              }{" "}
                                              {badgeSendingitems.user.last_name}
                                            </a>
                                          </small>
                                        </div>
                                      )
                                    )
                                  ) : (
                                    <></>
                                  )}
                                </div>

                                <div
                                  className="list-badge-prview mb-1"
                                  style={{
                                    backgroundImage: `URL(${backgroundImage})`,
                                    backgroundRepeat: "no-repeat",
                                    backgroundPosition: "center",
                                  }}
                                >
                                  <div className="badge-box">
                                    <img
                                      src={item?.BadgeImage?.BadgeImage}
                                      alt=""
                                    />
                                    <h5 className="font-weight-bolder mt-1">
                                      {item?.BadgeImage?.BadgeName}
                                    </h5>
                                  </div>
                                </div>
                                {/* <!--/ post img --> */}
                                {/* <!-- like share --> */}

                                <div className="row d-flex justify-content-start align-items-center flex-wrap pb-50 postmain">
                                  <div className="col-sm-6 d-flex justify-content-between justify-content-sm-start mb-2 postlike">
                                    <a
                                      href="javascript:void(0)"
                                      className="d-flex align-items-center text-muted text-nowrap"
                                    >
                                      {item?.likes?.items?.filter(
                                        (e) => e.likeUser.id === userID
                                      ).length > 0 ? (
                                        likeProcess === true ? (
                                          <Heart
                                            data-feather="heart"
                                            color="red"
                                            disabled
                                            className="profile-likes font-medium-3 mr-50 disabled"
                                          />
                                        ) : (
                                          <Heart
                                            data-feather="heart"
                                            color="red"
                                            className="profile-likes font-medium-3 mr-50"
                                            onClick={(e) => {
                                              if (
                                                BadgeModulePermissions.CreateBadgeLike ==
                                                true ||
                                                userRoleSelector == "SuperAdmin"
                                              ) {
                                                handleDeleteBadgeLike(
                                                  e,
                                                  item.likes.items
                                                );
                                              }
                                            }}
                                          />
                                        )
                                      ) : likeProcess === true ? (
                                        <Heart
                                          data-feather="heart"
                                          color="gray"
                                          disabled
                                          className="profile-likes font-medium-3 mr-50 disabled"
                                        />
                                      ) : (
                                        <Heart
                                          data-feather="heart"
                                          color="gray"
                                          className="profile-likes font-medium-3 mr-50"
                                          onClick={(e) => {
                                            if (
                                              BadgeModulePermissions.CreateBadgeLike ==
                                              true ||
                                              userRoleSelector == "SuperAdmin"
                                            ) {
                                              handleBadgeLike(
                                                e,
                                                item.id,
                                                item.likes.items
                                              );
                                            }
                                          }}
                                        />
                                      )}

                                      <span>{item.likes.items.length}</span>
                                    </a>

                                    {/* <!-- avatar group with tooltip --> */}
                                    {item.likes.items.length > 0 ? (
                                      <div className="d-flex align-items-center">
                                        <div
                                          className="avatar-group ml-1"
                                          key={i}
                                        >
                                          {item.likes.items
                                            .slice(0, 5)
                                            .map((likeitem, i) =>
                                              likeitem.likeUser.profileImage ===
                                                null ? (
                                                <div
                                                  data-toggle="tooltip"
                                                  data-popup="tooltip-custom"
                                                  data-placement="bottom"
                                                  className="avatar pull-up"
                                                  key={i}
                                                >
                                                  <img
                                                    src={userIcon}
                                                    height="26"
                                                    width="26"
                                                  />{" "}
                                                </div>
                                              ) : (
                                                <div
                                                  data-toggle="tooltip"
                                                  data-popup="tooltip-custom"
                                                  data-placement="bottom"
                                                  className="avatar pull-up"
                                                  key={i + likeitem.likeUser.id}
                                                >
                                                  <AmplifyS3Image
                                                    className="likedUserImages"
                                                    imgKey={
                                                      likeitem.likeUser
                                                        .profileImage
                                                    }
                                                    key={
                                                      i + likeitem.likeUser.id
                                                    }
                                                  />
                                                </div>
                                              )
                                            )}
                                        </div>
                                        {item.likes.items.length > 5 ? (
                                          <>
                                            <a
                                              href="javascript:void(0)"
                                              className="text-muted text-nowrap ml-50"
                                              onClick={() => {
                                                setlikeInfo(item.likes.items);
                                                setBadgeLikeModal(
                                                  !badgeLikeModal
                                                );
                                              }}
                                            >
                                              +More
                                            </a>
                                          </>
                                        ) : (
                                          ""
                                        )}{" "}
                                      </div>
                                    ) : (
                                      <></>
                                    )}

                                    {/* <!-- avatar group with tooltip --> */}
                                  </div>
                                  {/*                           <!-- share and like count and icons -->
                                   */}{" "}
                                  <div className="col-sm-6 d-flex justify-content-between justify-content-sm-end align-items-center mb-2 postcoment">
                                    <a
                                      href="javascript:void(0)"
                                      className="text-nowrap"
                                    >
                                      <MessageSquare
                                        data-feather="message-square"
                                        className="text-body font-medium-3 mr-50"
                                      ></MessageSquare>
                                      <span className="text-muted mr-1">
                                        {item.comments.items.length}
                                      </span>
                                    </a>
                                    <Share size="15px" onClick={(e) => { history.push("/listbadge/" + item?.id) }} />
                                  </div>
                                </div>

                                {/* <!-- like share --> */}

                                {/* <!-- comment box --> */}
                                {BadgeModulePermissions.CreateBadgeComment ==
                                  true || userRoleSelector == "SuperAdmin" ? (
                                  <div className="d-flex align-items-start mb-1">
                                    <div className="avatar mt-25 mr-75" key={i}>
                                      {userProfileImage === null ? (
                                        <img
                                          src={userIcon}
                                          height="40"
                                          width="40"
                                        />
                                      ) : (
                                        <>
                                          <AmplifyS3Image
                                            className="comment-profile-img"
                                            imgKey={userProfileImage}
                                          // theme={{
                                          //   photoImg: { width: "40px", height: "40px" },
                                          // }}
                                          />
                                        </>
                                      )}
                                    </div>
                                    <div className="profile-user-info w-100">
                                      <AvForm
                                        onSubmit={(e) => {
                                          setAddCommentSubmitted(true);
                                          handleAddBadgeComment(e, item.id);
                                        }}
                                      >
                                        <div className="d-flex align-items-center justify-content-between mb-1">
                                          <AvInput
                                            key={i}
                                            type="textarea"
                                            id={"comment" + i}
                                            name={"comment" + i}
                                            rows="1"
                                            autoComplete="off"
                                            maxlength="150"
                                            placeholder="Add Comment"
                                            value={
                                              AddCommentBlog == null
                                                ? ""
                                                : AddCommentBlog !== null &&
                                                  AddCommentBlog.id === item.id
                                                  ? commentContent
                                                  : ""
                                            }
                                            onChange={(e) => {
                                              setAddCommentBlog(item);
                                              // console.log("AddCommentBlog", item);
                                              setcommentContent(e.target.value);
                                              let comment = e.target.value;
                                              let newComment = comment
                                                .replace(/(\r\n|\n|\r)/gm, "")
                                                .trim();
                                              // console.log("comment", newComment);
                                              if (newComment != "") {
                                                setAddComment(true);
                                              } else {
                                                setAddComment(false);
                                              }
                                            }}
                                          />
                                        </div>
                                        {AddCommentBlog !== null &&
                                          AddComment == true &&
                                          AddCommentBlog.id == item.id ? (
                                          <>
                                            <Button
                                              type="submit"
                                              color="primary"
                                              className="btn btn-sm"
                                              disabled={AddCommentSubmitted}
                                            >
                                              Post Comment
                                            </Button>
                                            <Button
                                              type="cancel"
                                              className="btn btn-sm ml-1 "
                                              color="secondary"
                                              disabled={AddCommentSubmitted}
                                              onClick={(e) => {
                                                setcommentContent("");
                                                setAddCommentBlog(null);
                                                setAddComment(false);
                                              }}
                                            >
                                              Cancel
                                            </Button>
                                          </>
                                        ) : null}
                                      </AvForm>
                                    </div>
                                  </div>
                                ) : null}
                                {/* <!--/ comment box -->  */}
                                {/* {console.log("commentItem", item.comments.items[0])} */}
                                {item.comments.items.length > 0 ? (
                                  item.comments.items.map((commentItem, j) => (
                                    <div
                                      className="d-flex align-items-start mb-1"
                                      key={j}
                                    >
                                      <div
                                        className="avatar mt-25 mr-75"
                                        key={i + commentItem.commentUser.id}
                                      >
                                        {commentItem.commentUser
                                          .profileImage === null ? (
                                          <img
                                            src={userIcon}
                                            height="40"
                                            width="40"
                                          />
                                        ) : (
                                          <>
                                            <AmplifyS3Image
                                              className="comment-profile-img"
                                              imgKey={
                                                commentItem.commentUser
                                                  .profileImage
                                              }
                                            // theme={{
                                            //   photoImg: {
                                            //     width: "40px",
                                            //     height: "40px",
                                            //   },
                                            // }}
                                            />
                                          </>
                                        )}
                                      </div>
                                      <div className="profile-user-info w-100">
                                        <div className="d-flex align-items-center justify-content-between">
                                          <h6 className="mb-0">
                                            <a
                                              onClick={() =>
                                                history.push(
                                                  "/employee-profile/" +
                                                  commentItem.commentUser.id
                                                )
                                              }
                                            >
                                              {
                                                commentItem.commentUser
                                                  .first_name
                                              }{" "}
                                              {
                                                commentItem.commentUser
                                                  .last_name
                                              }
                                            </a>
                                          </h6>
                                          <div className="d-flex">
                                            <small className="mr-1">
                                              {moment(
                                                commentItem.createdAt
                                              ).fromNow()}
                                            </small>

                                            <span>
                                              {commentItem.commentUser.id ===
                                                userID ? (
                                                <UncontrolledDropdown>
                                                  <DropdownToggle
                                                    className="pr-1"
                                                    tag="span"
                                                  >
                                                    <MoreHorizontal size={15} />
                                                  </DropdownToggle>
                                                  <DropdownMenu right>
                                                    <DropdownItem
                                                      className="w-100"
                                                      onClick={(e) => {
                                                        setEditCommentMode(
                                                          true
                                                        );
                                                        setEditCommentData(
                                                          commentItem
                                                        );
                                                        setUpdatedComment(
                                                          commentItem.content
                                                        );
                                                        // console.log(
                                                        //   "item selected for edit",
                                                        //   commentItem
                                                        // );
                                                      }}
                                                    >
                                                      <span
                                                        className="align-middle ml-50 cursor-pointe"
                                                        onClick={() => {
                                                          setEditFlag(true);
                                                          setEditCommentInfo(
                                                            commentItem
                                                          );
                                                          setEditCommentModal(
                                                            !editCommentModal
                                                          );
                                                        }}
                                                      >
                                                        Edit
                                                      </span>
                                                    </DropdownItem>
                                                    <DropdownItem
                                                      className="w-100"
                                                      onClick={() => {
                                                        setDeleteCommentInfo(
                                                          commentItem
                                                        );
                                                        setDeleteModal(
                                                          !deleteModal
                                                        );
                                                      }}
                                                    >
                                                      <span className="align-middle ml-50 cursor-pointe">
                                                        Delete
                                                      </span>
                                                    </DropdownItem>
                                                  </DropdownMenu>
                                                </UncontrolledDropdown>
                                              ) : (
                                                <div></div>
                                              )}
                                            </span>
                                          </div>
                                        </div>

                                        <small>
                                          {EditCommentMode == true ? (
                                            EditCommentData.id ==
                                              commentItem.id ? (
                                              <>
                                                <div className="d-flex align-items-center justify-content-between mb-1">
                                                  <textarea
                                                    key={i}
                                                    className="form-control"
                                                    id={"comment" + i}
                                                    name={"comment" + i}
                                                    rows="1"
                                                    defaultValue={
                                                      commentItem.content
                                                    }
                                                    onChange={(e) => {
                                                      setCommentEdited(true);
                                                      setcommentReplyContent(
                                                        e.target.value
                                                      );
                                                      let comment =
                                                        e.target.value;
                                                      let newComment = comment
                                                        .replace(
                                                          /(\r\n|\n|\r)/gm,
                                                          ""
                                                        )
                                                        .trim();
                                                      // console.log(
                                                      //   "comment",
                                                      //   newComment
                                                      // );
                                                      if (newComment != "") {
                                                        setValidEditComment(
                                                          false
                                                        );
                                                      } else {
                                                        setValidEditComment(
                                                          true
                                                        );
                                                      }
                                                    }}
                                                  ></textarea>
                                                </div>
                                                {ValidEditComment === false ? (
                                                  <>
                                                    {commentUpdateProcess ===
                                                      true ? (
                                                      <button
                                                        type="button"
                                                        className="btn btn-sm btn-primary disabled"
                                                        disabled
                                                      >
                                                        Update
                                                      </button>
                                                    ) : (
                                                      <button
                                                        type="button"
                                                        className="btn btn-sm btn-primary"
                                                        onClick={(e) =>
                                                          handleEditComment(
                                                            commentItem
                                                          )
                                                        }
                                                      >
                                                        Update
                                                      </button>
                                                    )}

                                                    <button
                                                      type="button"
                                                      className="btn btn-sm btn-secondary ml-1"
                                                      onClick={(e) => {
                                                        setCommentEdited(false);
                                                        setEditCommentMode(
                                                          false
                                                        );
                                                        setEditCommentData(
                                                          null
                                                        );
                                                        setValidEditComment(
                                                          false
                                                        );
                                                      }}
                                                    >
                                                      Cancel
                                                    </button>
                                                  </>
                                                ) : null}
                                              </>
                                            ) : (
                                              commentItem.content
                                            )
                                          ) : (
                                            commentItem.content
                                          )}
                                        </small>
                                        <div>
                                          <a
                                            href="javascript:void(0)"
                                            className="mr-1"
                                          >
                                            {commentItem?.likes?.items?.filter(
                                              (e) => e.likeUser.id === userID
                                            ).length > 0 ? (
                                              commentLikeProcess === true ? (
                                                <Heart
                                                  className="text-body font-medium-1 mr-1 disabled"
                                                  color="red"
                                                  disabled
                                                ></Heart>
                                              ) : (
                                                <Heart
                                                  className="text-body font-medium-1 mr-1"
                                                  color="red"
                                                  onClick={(e) => {
                                                    handleDeleteBadgeCommentLike(
                                                      e,
                                                      commentItem.likes.items
                                                    );
                                                  }}
                                                ></Heart>
                                              )
                                            ) : commentLikeProcess === true ? (
                                              <Heart
                                                className="text-body font-medium-1 mr-1 disabled"
                                                color="gray"
                                                disabled
                                              ></Heart>
                                            ) : (
                                              <Heart
                                                className="text-body font-medium-1 mr-1"
                                                color="gray"
                                                onClick={(e) => {
                                                  handleBadgeCommentLike(
                                                    e,
                                                    commentItem.id,
                                                    commentItem.likes.items
                                                  );
                                                }}
                                              ></Heart>
                                            )}

                                            <small
                                              className="align-middle font-weight-bold primary text-muted"
                                              onClick={() => {
                                                setCommentLikeInfo(
                                                  commentItem.likes.items
                                                );
                                                setCommentLikeModal(
                                                  !commentLikeModal
                                                );
                                              }}
                                            >
                                              Like{" "}
                                              {commentItem.likes.items.length}
                                            </small>
                                          </a>

                                          {/*   <a
                                      href="javascript:void(0)"
                                      className="mr-1"
                                      onClick={() => {
                                        setBadgeNumber(i);
                                        setCommentReplyNumber(j);
                                      }}
                                    >
                                      <MessageSquare className="text-body font-medium-1 mr-1"></MessageSquare>
                                      <small className="align-middle font-weight-bold text-muted">
                                        Reply 0
                                      </small>
                                    </a> */}
                                        </div>

                                        {/* <!-- Sub comments --> 
                                         <div className="d-flex align-items-start mb-1 mt-1">
                                              <div className="avatar mt-25 mr-75">
                                                <img src={images7} alt="Avatar" height="34"
                                                  width="34" />
                                              </div>
                                              <div className="profile-user-info w-100">
                                                <div className="d-flex align-items-center justify-content-between">
                                                  <h6 className="mb-0">Kitty Allanson</h6>
                                                  <small>17h
  
                                                    <span>
                                                      <a id="dropdownMenuButton100" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i data-feather='more-horizontal'></i>
                                                      </a>
                                                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton100">
                                                        <a className="dropdown-item" href="javascript:void(0);">Edit</a>
                                                        <a className="dropdown-item" href="javascript:void(0);">Delete</a>
                                                      </div>
                                                    </span>
  
                                                  </small>
                                                </div>
  
                                                <small>Easy & smart fuzzy searchÃ°Å¸â€¢ÂµÃ°Å¸ÂÂ» functionality which enables users to search
                                                  quickly.</small>
                                                <div>
                                                  <a href="javascript:void(0)" className="mr-2">
                                                    <i data-feather="heart" className="text-body font-medium-1"></i>
                                                    <small className="align-middle font-weight-bold primary text-muted">Like 34</small>
                                                  </a>
  
                                                  <a href="javascript:void(0)">
                                                    <i data-feather="message-square" className="text-body font-medium-1"></i>
                                                    <small className="align-middle font-weight-bold text-muted">Reply 0</small>
                                                  </a>
                                                </div>
  
                                                <!-- comment box --> 
                                                <div className="d-flex align-items-start mt-2">
                                                  <div className="avatar mt-25 mr-75">
                                                    <img src={image18} alt="Avatar"
                                                      height="34" width="34" />
                                                  </div>
                                                  <div className="profile-user-info w-100">
                                                    <div className="d-flex align-items-center justify-content-between mb-1">
                                                      <textarea className="form-control" id="label-textarea" rows="1"
                                                        placeholder="Add a reply..."></textarea>
                                                    </div>
                                                    <button type="button" className="btn btn-sm btn-primary">Reply</button>
                                                  </div>
                                                </div>
                                                <!--/ comment box --> 
  
                                              </div>
                                          </div>
                         <!-- End Sub comments -->  */}

                                        {/* <!-- comment box --> */}
                                        {commentReplyNumber === j &&
                                          badgeNumber === i ? (
                                          <div className="d-flex align-items-start mt-2">
                                            <div
                                              className="avatar mt-25 mr-75"
                                              key={i}
                                            >
                                              {userProfileImage === null ? (
                                                <img
                                                  src={userIcon}
                                                  height="40"
                                                  width="40"
                                                />
                                              ) : (
                                                <>
                                                  <AmplifyS3Image
                                                    className="badge-profile-img"
                                                    imgKey={userProfileImage}
                                                  // theme={{
                                                  //   photoImg: { width: "40px", height: "40px" },
                                                  // }}
                                                  />
                                                </>
                                              )}
                                            </div>
                                            <div className="profile-user-info w-100">
                                              <div className="d-flex align-items-center justify-content-between mb-1">
                                                <textarea
                                                  className="form-control"
                                                  id="label-textarea"
                                                  rows="1"
                                                  placeholder="Add a reply..."
                                                  onChange={(e) => {
                                                    setcommentReply(
                                                      e.target.value
                                                    );
                                                  }}
                                                ></textarea>
                                              </div>
                                              <button
                                                type="button"
                                                className="btn btn-sm btn-primary"
                                                onClick={(e) =>
                                                  handleCommentReply(
                                                    e,
                                                    item.id,
                                                    commentItem.id
                                                  )
                                                }
                                              >
                                                Reply
                                              </button>
                                            </div>
                                          </div>
                                        ) : (
                                          <></>
                                        )}

                                        {/* <!--/ comment box --> */}
                                      </div>
                                    </div>
                                  ))
                                ) : (
                                  <div></div>
                                )}
                              </div>
                            </div>
                          ))
                        ) : (
                          myBadgeArr.map((item, i) => (
                            <div className="card profileimagetest " key={i}>
                              <div className="card-body">
                                <div className="d-flex justify-content-between align-items-start">
                                  <div className="d-flex justify-content-start align-items-center mb-1">
                                    {/* <!-- avatar --> */}
                                    <div
                                      className="avatar mr-1"
                                      key={i + item.createdByUser.id}
                                    >
                                      {item.createdByUser.profileImage ===
                                        null ? (
                                        <img
                                          src={userIcon}
                                          height="50"
                                          width="50"
                                        />
                                      ) : (
                                        <>
                                          <AmplifyS3Image
                                            imgKey={
                                              item.createdByUser.profileImage
                                            }
                                            className="badge-profile-img"
                                          />
                                        </>
                                      )}
                                    </div>
                                    {/* <!--/ avatar --> */}
                                    <div className="profile-user-info">
                                      <h6 className="mb-0">
                                        <a
                                          onClick={() =>
                                            history.push(
                                              "/employee-profile/" +
                                              item.createdByUser.id
                                            )
                                          }
                                        >
                                          {item.createdByUser.first_name}{" "}
                                          {item.createdByUser.last_name}
                                        </a>
                                      </h6>
                                      <small className="card-text">
                                        Send Recognition -{" "}
                                        {moment(item.createdAt).fromNow()}
                                      </small>
                                    </div>
                                  </div>
                                  {/*<button type="button" className="btn btn-outline-primary btn-sm round waves-effect">Publish</button>
                                   */}
                                </div>

                                {!read ? (
                                  item.description.length > 100 ? (
                                    <p className="card-text">
                                      {item.description.slice(0, 100)} . . . .{" "}
                                    </p>
                                  ) : (
                                    <p className="card-text">
                                      {item.description}
                                    </p>
                                  )
                                ) : (
                                  <p className="card-text" id={`project${i}`}>
                                    {item.description}
                                  </p>
                                )}
                                {item.description.length > 100 ? (
                                  <div className="w-100 text-right mb-1">
                                    <a
                                      href="#"
                                      data-toggle="collapse"
                                      href={`#project${i}`}
                                      role="button"
                                      onClick={() => readmore(i)}
                                      aria-expanded="false"
                                      aria-controls="collapseExample"
                                      className="w-100 text-right mb-1"
                                    >
                                      {read && i === index1
                                        ? "Read Less"
                                        : "Read More"}
                                    </a>
                                  </div>
                                ) : null}

                                {/* <div className="w-100 text-right mb-1">
                          <a href="#">...see more</a>
                        </div> */}
                                {/* <!-- post img --> */}
                                {item.isPublic === true ? (
                                  <div className="d-flex flex-wrap badge-users">
                                    {item.BadgeSending.items.length > 0 ? (
                                      item.BadgeSending.items.map(
                                        (badgeSendingitems, i) => (
                                          <div
                                            className="px-1 b-users text-center mb-2"
                                            key={i}
                                          >
                                            <div
                                              className="avatar avatar-lg"
                                              key={
                                                i + badgeSendingitems.user.id
                                              }
                                            >
                                              {badgeSendingitems.user
                                                .profileImage === null ? (
                                                <img
                                                  src={userIcon}
                                                  height="50"
                                                  width="50"
                                                />
                                              ) : (
                                                <>
                                                  <AmplifyS3Image
                                                    imgKey={
                                                      badgeSendingitems.user
                                                        .profileImage
                                                    }
                                                    className="badge-profile-img"
                                                  />
                                                </>
                                              )}
                                            </div>
                                            <small className="d-block">
                                              <a
                                                onClick={() =>
                                                  history.push(
                                                    "/employee-profile/" +
                                                    badgeSendingitems.user.id
                                                  )
                                                }
                                              >
                                                {
                                                  badgeSendingitems.user
                                                    .first_name
                                                }{" "}
                                                {
                                                  badgeSendingitems.user
                                                    .last_name
                                                }
                                              </a>
                                            </small>
                                          </div>
                                        )
                                      )
                                    ) : (
                                      <></>
                                    )}
                                  </div>
                                ) : (
                                  <></>
                                )}

                                <div
                                  className="list-badge-prview mb-1"
                                  style={{
                                    backgroundImage: `URL(${backgroundImage})`,
                                    backgroundRepeat: "no-repeat",
                                    backgroundPosition: "center",
                                  }}
                                >
                                  <div className="badge-box">
                                    <img
                                      src={item.BadgeImage.BadgeImage}
                                      alt=""
                                    />
                                    <h5 className="font-weight-bolder mt-1">
                                      {item.BadgeImage.BadgeName}
                                    </h5>
                                  </div>
                                </div>
                                {/* <!--/ post img --> */}
                                {/* <!-- like share --> */}
                                {item.isPublic === true ? (
                                  <div className="row d-flex justify-content-start align-items-center flex-wrap pb-50">
                                    <div className="col-sm-6 d-flex justify-content-between justify-content-sm-start mb-2">
                                      <a
                                        href="javascript:void(0)"
                                        className="d-flex align-items-center text-muted text-nowrap"
                                      >
                                        <i
                                          data-feather="heart"
                                          className="profile-likes font-medium-3 mr-50"
                                        ></i>
                                        {item?.likes?.items?.filter(
                                          (e) => e.likeUser.id === userID
                                        ).length > 0 ? (
                                          likeProcess === true ? (
                                            <Heart
                                              data-feather="heart"
                                              color="red"
                                              disabled
                                              className="profile-likes font-medium-3 mr-50 disabled"
                                            />
                                          ) : (
                                            <Heart
                                              data-feather="heart"
                                              color="red"
                                              className="profile-likes font-medium-3 mr-50"
                                              onClick={(e) => {
                                                handleDeleteBadgeLike(
                                                  e,
                                                  item.likes.items
                                                );
                                              }}
                                            />
                                          )
                                        ) : likeProcess === true ? (
                                          <Heart
                                            data-feather="heart"
                                            color="gray"
                                            disabled
                                            className="profile-likes font-medium-3 mr-50 disabled"
                                          />
                                        ) : (
                                          <Heart
                                            data-feather="heart"
                                            color="gray"
                                            className="profile-likes font-medium-3 mr-50"
                                            onClick={(e) => {
                                              handleBadgeLike(
                                                e,
                                                item.id,
                                                item.likes.items
                                              );
                                            }}
                                          />
                                        )}

                                        <span>{item.likes.items.length}</span>
                                      </a>

                                      {/* <!-- avatar group with tooltip --> */}
                                      {item.likes.items.length > 0 ? (
                                        <div className="d-flex align-items-center">
                                          <div
                                            className="avatar-group ml-1"
                                            key={i}
                                          >
                                            {item.likes.items
                                              .slice(0, 5)
                                              .map((likeitem, i) =>
                                                likeitem.likeUser
                                                  .profileImage === null ? (
                                                  <div
                                                    data-toggle="tooltip"
                                                    data-popup="tooltip-custom"
                                                    data-placement="bottom"
                                                    className="avatar pull-up"
                                                    key={i}
                                                  >
                                                    <img
                                                      src={userIcon}
                                                      height="26"
                                                      width="26"
                                                    />{" "}
                                                  </div>
                                                ) : (
                                                  <div
                                                    data-toggle="tooltip"
                                                    data-popup="tooltip-custom"
                                                    data-placement="bottom"
                                                    className="avatar pull-up"
                                                    key={
                                                      i + likeitem.likeUser.id
                                                    }
                                                  >
                                                    <AmplifyS3Image
                                                      className="likedUserImages"
                                                      imgKey={
                                                        likeitem.likeUser
                                                          .profileImage
                                                      }
                                                    />
                                                  </div>
                                                )
                                              )}
                                          </div>
                                          {item.likes.items.length > 5 ? (
                                            <>
                                              <a
                                                href="javascript:void(0)"
                                                className="text-muted text-nowrap ml-50"
                                                onClick={() => {
                                                  setlikeInfo(item.likes.items);
                                                  setBadgeLikeModal(
                                                    !badgeLikeModal
                                                  );
                                                }}
                                              >
                                                +More
                                              </a>
                                            </>
                                          ) : (
                                            ""
                                          )}{" "}
                                        </div>
                                      ) : (
                                        <></>
                                      )}

                                      {/* <!-- avatar group with tooltip --> */}
                                    </div>
                                    {/*                           <!-- share and like count and icons -->
                                     */}{" "}
                                    <div className="col-sm-6 d-flex justify-content-between justify-content-sm-end align-items-center mb-2">
                                      <a
                                        href="javascript:void(0)"
                                        className="text-nowrap"
                                      >
                                        <MessageSquare
                                          data-feather="message-square"
                                          className="text-body font-medium-3 mr-50"
                                        ></MessageSquare>
                                        <span className="text-muted mr-1">
                                          {item.comments.items.length}
                                        </span>
                                      </a>

                                      {/* <a href="javascript:void(0)" className="text-nowrap">
                              <i data-feather="share-2" className="text-body font-medium-3 mx-50"></i>
                              <span className="text-muted">1.25k</span>
                            </a> */}
                                    </div>
                                    {/*                     <!-- share and like count and icons -->
                                     */}
                                  </div>
                                ) : (
                                  <></>
                                )}
                                {/* <!-- like share --> */}

                                {/* <!-- comment box --> */}
                                {item.isPublic === true ? (
                                  <div className="d-flex align-items-start mb-1">
                                    <div className="avatar mt-25 mr-75" key={i}>
                                      {userProfileImage === null ? (
                                        <img
                                          src={userIcon}
                                          height="40"
                                          width="40"
                                        />
                                      ) : (
                                        <>
                                          <AmplifyS3Image
                                            className="comment-profile-img"
                                            imgKey={userProfileImage}
                                          // theme={{
                                          //   photoImg: { width: "40px", height: "40px" },
                                          // }}
                                          />
                                        </>
                                      )}
                                    </div>
                                    <div className="profile-user-info w-100">
                                      <AvForm
                                        onSubmit={(e) => {
                                          setAddCommentSubmitted(true);
                                          handleAddBadgeComment(e, item.id);
                                        }}
                                      >
                                        <div className="d-flex align-items-center justify-content-between mb-1">
                                          <AvInput
                                            key={i}
                                            type="textarea"
                                            id={"comment" + i}
                                            name={"comment" + i}
                                            rows="1"
                                            autoComplete="off"
                                            maxlength="150"
                                            placeholder="Add Comment"
                                            value={
                                              AddCommentBlog == null
                                                ? ""
                                                : AddCommentBlog !== null &&
                                                  AddCommentBlog.id === item.id
                                                  ? commentContent
                                                  : ""
                                            }
                                            onChange={(e) => {
                                              setAddCommentBlog(item);
                                              // console.log("AddCommentBlog", item);
                                              setcommentContent(e.target.value);
                                              let comment = e.target.value;
                                              let newComment = comment
                                                .replace(/(\r\n|\n|\r)/gm, "")
                                                .trim();
                                              // console.log("comment", newComment);
                                              if (newComment != "") {
                                                setAddComment(true);
                                              } else {
                                                setAddComment(false);
                                              }
                                            }}
                                          />
                                        </div>
                                        {AddCommentBlog !== null &&
                                          AddComment == true &&
                                          AddCommentBlog.id == item.id ? (
                                          <>
                                            <Button
                                              type="submit"
                                              color="primary"
                                              className="btn btn-sm"
                                              disabled={AddCommentSubmitted}
                                            >
                                              Post Comment
                                            </Button>
                                            <Button
                                              type="cancel"
                                              className="btn btn-sm ml-1 "
                                              color="secondary"
                                              disabled={AddCommentSubmitted}
                                              onClick={(e) => {
                                                setcommentContent("");
                                                setAddCommentBlog(null);
                                                setAddComment(false);
                                              }}
                                            >
                                              Cancel
                                            </Button>
                                          </>
                                        ) : null}
                                      </AvForm>
                                    </div>
                                  </div>
                                ) : (
                                  <></>
                                )}

                                {/* <!--/ comment box -->  */}
                                {/* {console.log("commentItem", item.comments.items[0])} */}
                                {item.comments.items.length > 0 ? (
                                  item.comments.items.map((commentItem, j) => (
                                    <div
                                      className="d-flex align-items-start mb-1"
                                      key={j}
                                    >
                                      {item.isPublic === true ? (
                                        <div
                                          className="avatar mt-25 mr-75"
                                          key={i + commentItem.commentUser.id}
                                        >
                                          {commentItem.commentUser
                                            .profileImage === null ? (
                                            <img
                                              src={userIcon}
                                              height="40"
                                              width="40"
                                            />
                                          ) : (
                                            <>
                                              <AmplifyS3Image
                                                className="comment-profile-img"
                                                imgKey={
                                                  commentItem.commentUser
                                                    .profileImage
                                                }
                                              // theme={{
                                              //   photoImg: {
                                              //     width: "40px",
                                              //     height: "40px",
                                              //   },
                                              // }}
                                              />
                                            </>
                                          )}
                                        </div>
                                      ) : (
                                        <></>
                                      )}
                                      {item.isPublic === true ? (
                                        <div className="profile-user-info w-100">
                                          <div className="d-flex align-items-center justify-content-between">
                                            <h6 className="mb-0">
                                              <a
                                                onClick={() =>
                                                  history.push(
                                                    "/employee-profile/" +
                                                    commentItem.commentUser.id
                                                  )
                                                }
                                              >
                                                {
                                                  commentItem.commentUser
                                                    .first_name
                                                }{" "}
                                                {
                                                  commentItem.commentUser
                                                    .last_name
                                                }
                                              </a>
                                            </h6>
                                            <div className="d-flex">
                                              <small className="mr-1">
                                                {moment(
                                                  commentItem.createdAt
                                                ).fromNow()}
                                              </small>

                                              <span>
                                                {commentItem.commentUser.id ===
                                                  userID ? (
                                                  <UncontrolledDropdown>
                                                    <DropdownToggle
                                                      className="pr-1"
                                                      tag="span"
                                                    >
                                                      <MoreHorizontal
                                                        size={15}
                                                      />
                                                    </DropdownToggle>
                                                    <DropdownMenu right>
                                                      <DropdownItem
                                                        className="w-100"
                                                        onClick={(e) => {
                                                          setEditCommentMode(
                                                            true
                                                          );
                                                          setEditCommentData(
                                                            commentItem
                                                          );
                                                          setUpdatedComment(
                                                            commentItem.content
                                                          );
                                                          // console.log(
                                                          //   "item selected for edit",
                                                          //   commentItem
                                                          // );
                                                        }}
                                                      >
                                                        <span
                                                          className="align-middle ml-50 cursor-pointe"
                                                          onClick={() => {
                                                            setEditFlag(true);
                                                            setEditCommentInfo(
                                                              commentItem
                                                            );
                                                            setEditCommentModal(
                                                              !editCommentModal
                                                            );
                                                          }}
                                                        >
                                                          Edit
                                                        </span>
                                                      </DropdownItem>
                                                      <DropdownItem className="w-100">
                                                        <span
                                                          className="align-middle ml-50 cursor-pointe"
                                                          onClick={() => {
                                                            setDeleteCommentInfo(
                                                              commentItem
                                                            );
                                                            setDeleteModal(
                                                              !deleteModal
                                                            );
                                                          }}
                                                        >
                                                          Delete
                                                        </span>
                                                      </DropdownItem>
                                                    </DropdownMenu>
                                                  </UncontrolledDropdown>
                                                ) : (
                                                  <div></div>
                                                )}
                                              </span>
                                            </div>
                                          </div>

                                          <small>
                                            {EditCommentMode == true ? (
                                              EditCommentData.id ==
                                                commentItem.id ? (
                                                <>
                                                  <div className="d-flex align-items-center justify-content-between mb-1">
                                                    <textarea
                                                      key={i}
                                                      className="form-control"
                                                      id={"comment" + i}
                                                      name={"comment" + i}
                                                      rows="1"
                                                      defaultValue={
                                                        commentItem.content
                                                      }
                                                      onChange={(e) => {
                                                        setCommentEdited(true);
                                                        setcommentReplyContent(
                                                          e.target.value
                                                        );
                                                        let comment =
                                                          e.target.value;
                                                        let newComment = comment
                                                          .replace(
                                                            /(\r\n|\n|\r)/gm,
                                                            ""
                                                          )
                                                          .trim();
                                                        // console.log(
                                                        //   "comment",
                                                        //   newComment
                                                        // );
                                                        if (newComment != "") {
                                                          setValidEditComment(
                                                            false
                                                          );
                                                        } else {
                                                          setValidEditComment(
                                                            true
                                                          );
                                                        }
                                                      }}
                                                    ></textarea>
                                                  </div>
                                                  {ValidEditComment ===
                                                    false ? (
                                                    <>
                                                      {commentUpdateProcess ===
                                                        true ? (
                                                        <button
                                                          type="button"
                                                          className="btn btn-sm btn-primary disabled"
                                                          disabled
                                                        >
                                                          Update
                                                        </button>
                                                      ) : (
                                                        <button
                                                          type="button"
                                                          className="btn btn-sm btn-primary"
                                                          onClick={(e) =>
                                                            handleEditComment(
                                                              commentItem
                                                            )
                                                          }
                                                        >
                                                          Update
                                                        </button>
                                                      )}
                                                      <button
                                                        type="button"
                                                        className="btn btn-sm btn-secondary ml-1"
                                                        onClick={(e) => {
                                                          setCommentEdited(
                                                            false
                                                          );
                                                          setEditCommentMode(
                                                            false
                                                          );
                                                          setEditCommentData(
                                                            null
                                                          );
                                                          setValidEditComment(
                                                            false
                                                          );
                                                        }}
                                                      >
                                                        Cancel
                                                      </button>
                                                    </>
                                                  ) : null}
                                                </>
                                              ) : (
                                                commentItem.content
                                              )
                                            ) : (
                                              commentItem.content
                                            )}
                                          </small>
                                          <div>
                                            <a
                                              href="javascript:void(0)"
                                              className="mr-1"
                                            >
                                              {commentItem?.likes?.items?.filter(
                                                (e) => e.likeUser.id === userID
                                              ).length > 0 ? (
                                                commentLikeProcess === true ? (
                                                  <Heart
                                                    className="text-body font-medium-1 mr-1 disabled"
                                                    color="red"
                                                    disabled
                                                  ></Heart>
                                                ) : (
                                                  <Heart
                                                    className="text-body font-medium-1 mr-1"
                                                    color="red"
                                                    onClick={(e) => {
                                                      handleDeleteBadgeCommentLike(
                                                        e,
                                                        commentItem.likes.items
                                                      );
                                                    }}
                                                  ></Heart>
                                                )
                                              ) : commentLikeProcess ===
                                                true ? (
                                                <Heart
                                                  className="text-body font-medium-1 mr-1 disabled"
                                                  color="gray"
                                                  disabled
                                                ></Heart>
                                              ) : (
                                                <Heart
                                                  className="text-body font-medium-1 mr-1"
                                                  color="gray"
                                                  onClick={(e) => {
                                                    handleBadgeCommentLike(
                                                      e,
                                                      commentItem.id,
                                                      commentItem.likes.items
                                                    );
                                                  }}
                                                ></Heart>
                                              )}

                                              <small
                                                className="align-middle font-weight-bold primary text-muted"
                                                onClick={() => {
                                                  setCommentLikeInfo(
                                                    commentItem.likes.items
                                                  );
                                                  setCommentLikeModal(
                                                    !commentLikeModal
                                                  );
                                                }}
                                              >
                                                Like{" "}
                                                {commentItem.likes.items.length}
                                              </small>
                                            </a>

                                            {/*                                     
                                        <a
                                        href="javascript:void(0)"
                                        className="mr-1"
                                        onClick={() => {
                                          setBadgeNumber(i);
                                          setCommentReplyNumber(j);
                                        }}
                                      >
                                        <MessageSquare className="text-body font-medium-1 mr-1"></MessageSquare>
                                        <small className="align-middle font-weight-bold text-muted">
                                          Reply 0
                                        </small>
                                      </a> */}
                                          </div>

                                          {/* <!-- Sub comments --> 
                                       <div className="d-flex align-items-start mb-1 mt-1">
                                            <div className="avatar mt-25 mr-75">
                                              <img src={images7} alt="Avatar" height="34"
                                                width="34" />
                                            </div>
                                            <div className="profile-user-info w-100">
                                              <div className="d-flex align-items-center justify-content-between">
                                                <h6 className="mb-0">Kitty Allanson</h6>
                                                <small>17h

                                                  <span>
                                                    <a id="dropdownMenuButton100" data-toggle="dropdown" aria-haspopup="true"
                                                      aria-expanded="false">
                                                      <i data-feather='more-horizontal'></i>
                                                    </a>
                                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton100">
                                                      <a className="dropdown-item" href="javascript:void(0);">Edit</a>
                                                      <a className="dropdown-item" href="javascript:void(0);">Delete</a>
                                                    </div>
                                                  </span>

                                                </small>
                                              </div>

                                              <small>Easy & smart fuzzy searchÃ°Å¸â€¢ÂµÃ°Å¸ÂÂ» functionality which enables users to search
                                                quickly.</small>
                                              <div>
                                                <a href="javascript:void(0)" className="mr-2">
                                                  <i data-feather="heart" className="text-body font-medium-1"></i>
                                                  <small className="align-middle font-weight-bold primary text-muted">Like 34</small>
                                                </a>

                                                <a href="javascript:void(0)">
                                                  <i data-feather="message-square" className="text-body font-medium-1"></i>
                                                  <small className="align-middle font-weight-bold text-muted">Reply 0</small>
                                                </a>
                                              </div>

                                              <!-- comment box --> 
                                              <div className="d-flex align-items-start mt-2">
                                                <div className="avatar mt-25 mr-75">
                                                  <img src={image18} alt="Avatar"
                                                    height="34" width="34" />
                                                </div>
                                                <div className="profile-user-info w-100">
                                                  <div className="d-flex align-items-center justify-content-between mb-1">
                                                    <textarea className="form-control" id="label-textarea" rows="1"
                                                      placeholder="Add a reply..."></textarea>
                                                  </div>
                                                  <button type="button" className="btn btn-sm btn-primary">Reply</button>
                                                </div>
                                              </div>
                                              <!--/ comment box --> 

                                            </div>
                                        </div>
                                   <!-- End Sub comments -->  */}

                                          {/* <!-- comment box --> */}
                                          {commentReplyNumber === j &&
                                            badgeNumber === i ? (
                                            <div className="d-flex align-items-start mt-2">
                                              <div
                                                className="avatar mt-25 mr-75"
                                                key={i}
                                              >
                                                {userProfileImage === null ? (
                                                  <img
                                                    src={userIcon}
                                                    height="40"
                                                    width="40"
                                                  />
                                                ) : (
                                                  <>
                                                    <AmplifyS3Image
                                                      className="badge-profile-img"
                                                      imgKey={userProfileImage}
                                                    // theme={{
                                                    //   photoImg: { width: "40px", height: "40px" },
                                                    // }}
                                                    />
                                                  </>
                                                )}
                                              </div>
                                              <div className="profile-user-info w-100">
                                                <div className="d-flex align-items-center justify-content-between mb-1">
                                                  <textarea
                                                    className="form-control"
                                                    id="label-textarea"
                                                    rows="1"
                                                    placeholder="Add a reply..."
                                                    onChange={(e) => {
                                                      setcommentReply(
                                                        e.target.value
                                                      );
                                                    }}
                                                  ></textarea>
                                                </div>
                                                <button
                                                  type="button"
                                                  className="btn btn-sm btn-primary"
                                                  onClick={(e) =>
                                                    handleCommentReply(
                                                      e,
                                                      item.id,
                                                      commentItem.id
                                                    )
                                                  }
                                                >
                                                  Reply
                                                </button>
                                              </div>
                                            </div>
                                          ) : (
                                            <></>
                                          )}

                                          {/* <!--/ comment box --> */}
                                        </div>
                                      ) : (
                                        <></>
                                      )}
                                    </div>
                                  ))
                                ) : (
                                  <div></div>
                                )}
                              </div>
                            </div>
                          ))
                        )
                      ) : (
                        <div style={{ textAlign: "center" }}>
                          {loadProcess === true ? (
                            <Spinner color="dark" size="sm">
                              Loading...
                            </Spinner>
                          ) : (
                            <CardBody className="text-center">
                              <div className="text-center cursor-pointer">
                                <img src={EmptyIcon} width="150px" />{" "}
                              </div>
                              <div className="text-center">
                                <p style={{ color: "#dfe8ee" }}>
                                  No recognitions available
                                </p>
                              </div>
                            </CardBody>
                          )}
                        </div>
                      )}

                      {/* <!-- post 1 --> */}
                    </div>
                    {/* <!--/ center profile info section --> */}

                    {/* <!-- right profile info section --> */}
                    {!props.empID ? (
                      <div className="col-lg-3 col-12 order-3">
                        {AnnouncementModulePermissions?.ViewAnnouncement ==
                          true || userRoleSelector == "SuperAdmin" ? (
                          <CompanyAnnouncement />
                        ) : null}
                        {BlogModulePermissions?.ViewPost == true ||
                          userRoleSelector == "SuperAdmin" ? (
                          <CompanyNews CognitoInfo={cognitoUser} />
                        ) : null}
                      </div>
                    ) : (
                      <></>
                    )}
                    {/* <!--/ right profile info section --> */}
                  </div>
                </section>
              </div>
            </div>
          </div>
        ) : (
          <div className="text-center">
            You do not have enough permissions to view recognitions!
          </div>
        )
      ) : (
        <div className="text-center">
          <Spinner size="sm" className="mr-1" />
        </div>
      )}{" "}
    </Fragment>
  );
};

export default ListAllBadges;
