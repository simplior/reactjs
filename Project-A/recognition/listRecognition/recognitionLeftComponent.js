import React from "react";
import { Card, CardHeader, CardBody} from 'reactstrap'
import { useSelector } from "react-redux";
import {useHistory} from "react-router-dom"

const BadgeCard = () => {
    var myBadgeArr = useSelector((state) => state.badgeReducer).badgearr;
    const history = useHistory();

    const renderBadges = () => {
        return(
          <div >
            <div className="my-badges-short d-flex flex-wrap ">
            {myBadgeArr.length > 0 ? (
              myBadgeArr.map((item) => (
                <div className="box">
                  <span className="badge-icon avatar bg-light-primary">
                    <img src={item.BadgeImage.BadgeImage} alt="" />
                  </span>
                  <h6 className="font-weight-bolder mt-1">
                    {item.BadgeImage.BadgeName}
                  </h6>
                  <small>
                    By {item.createdByUser.first_name}{" "}
                    {item.createdByUser.last_name}
                  </small>
                </div>
              ))
            ) : (
              <></>
            )}
          </div>
          </div>
            
        )
    }
    return(
       <Card>
           <CardHeader tag='h5'>My Recognitions
           <a onClick={() => {
                   history.push({ pathname: "/listallbadges", state: {allBadges:false,myBadges:true} });
           }}>View All</a>
           </CardHeader>
           
          <CardBody>{renderBadges()}</CardBody>
       </Card>
    )
}

export default BadgeCard