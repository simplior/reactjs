import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import BadgeService from "../../../services/badge.service";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { AmplifyS3Image } from "@aws-amplify/ui-react";
import userIcon from "@src/assets/images/avatars/icon_user.png";
import moment from "moment";
import backgroundImage from "../../../../src/assets/images/icons/badge-bg.png";
import { Helmet } from "react-helmet";

function SingleBadge() {
    var { id } = useParams()
    const [badge, setBadge] = useState([]);
    const [read, setRead] = useState(false);
    const [index1, setIndex1] = useState(null);
    const history = useHistory()
    const readmore = (i) => {
        setIndex1(i);
        setRead(!read);
    };
    useEffect(async () => {
        try {
            var badges = await BadgeService.listAllBadgesByID(id);
            setBadge(badges?.data?.listBadges?.items)
        }
        catch (err) {
            console.log(err);
        }
    }, [])
    console.log("badges=", badge)
    return (
        badge?.map((item, i) => (
            <div className="card profileimagetest" key={i} style={{ width: "50%", margin: "25px 25%" }}>
                <Helmet>
                    <title>Recognition</title>
                    <meta name="title" content={item?.BadgeImage?.BadgeName} />
                    <meta name="description" content={"I got a badge from " + item.createdByUser.first_name + " click to see more."} />
                    {/*Facbeook*/}
                    <meta property="og:type" content="website" />
                    <meta property="og:url" content="https://emplify.app" />
                    <meta property="og:title" content={item?.BadgeImage?.BadgeName} />
                    <meta property="og:description" content={"I got a badge from " + item.createdByUser.first_name + " click to see more."} />
                    <meta property="og:image" content={item?.BadgeImage?.BadgeImage}></meta>
                    <meta property="og:image:width" content="200" />
                    <meta property="og:image:height" content="200" />
                    {/*Twitter*/}
                    {/* <meta property="twitter:card" content="summary_large_image" />
                    <meta property="twitter:url" content="https://metatags.io/" />
                    <meta property="twitter:title" content={item?.BadgeImage?.BadgeName} />
                    <meta property="twitter:description" content={"I got a badge from " + item.createdByUser.first_name + " click to see more."} />
                    <meta property="twitter:image" content={item?.BadgeImage?.BadgeImage} ></meta> */}
                </Helmet>
                <div className="card-body">
                    <div className="d-flex justify-content-between align-items-start">
                        <div className="d-flex justify-content-start align-items-center mb-1">
                            {/* <!-- avatar --> */}
                            <div
                                className="avatar mr-1"
                                key={i + item.createdByUser.id}
                            >
                                {item.createdByUser.profileImage ===
                                    null ? (
                                    <img
                                        src={userIcon}
                                        height="50"
                                        width="50"
                                    />
                                ) : (
                                    <>
                                        <AmplifyS3Image
                                            className="badge-profile-img"
                                            imgKey={
                                                item.createdByUser.profileImage
                                            }
                                        />
                                    </>
                                )}
                            </div>
                            {/* <!--/ avatar --> */}
                            <div className="profile-user-info">
                                <h6 className="mb-0">
                                    <a
                                        onClick={() =>
                                            history.push(
                                                "/employee-profile/" +
                                                item.createdByUser.id
                                            )
                                        }
                                    >
                                        {item.createdByUser.first_name}{" "}
                                        {item.createdByUser.last_name}
                                    </a>
                                </h6>
                                <small className="card-text">
                                    Send Recognition -{" "}
                                    {moment(item.createdAt).fromNow()}
                                </small>
                            </div>
                        </div>
                        {/*<button type="button" className="btn btn-outline-primary btn-sm round waves-effect">Publish</button>
                   */}
                    </div>

                    {!read ? (
                        item.description.length > 100 ? (
                            <p className="card-text">
                                {item.description.slice(0, 100)} . . . .{" "}
                            </p>
                        ) : (
                            <p className="card-text">
                                {item.description}
                            </p>
                        )
                    ) : (
                        <p className="card-text" id={`project${i}`}>
                            {item.description}
                        </p>
                    )}
                    {item.description.length > 100 ? (
                        <div className="w-100 text-right mb-1">
                            <a
                                href="#"
                                data-toggle="collapse"
                                href={`#project${i}`}
                                role="button"
                                onClick={() => readmore(i)}
                                aria-expanded="false"
                                aria-controls="collapseExample"
                                className="w-100 text-right mb-1"
                            >
                                {read && i === index1
                                    ? "Read Less"
                                    : "Read More"}
                            </a>
                        </div>
                    ) : null}

                    {/* <div className="w-100 text-right mb-1">
            <a href="#">...see more</a>
          </div> */}
                    {/* <!-- post img --> */}
                    <div className="d-flex flex-wrap badge-users">
                        {item.BadgeSending.items.length > 0 ? (
                            item.BadgeSending.items.map(
                                (badgeSendingitems, i) => (
                                    <div
                                        className="px-1 b-users text-center mb-2"
                                        key={i}
                                    >
                                        <div
                                            className="avatar avatar-lg"
                                            key={i + badgeSendingitems.user.id}
                                        >
                                            {badgeSendingitems.user
                                                .profileImage === null ? (
                                                <img
                                                    src={userIcon}
                                                    height="50"
                                                    width="50"
                                                />
                                            ) : (
                                                <>
                                                    <AmplifyS3Image
                                                        imgKey={
                                                            badgeSendingitems.user
                                                                .profileImage
                                                        }
                                                        className="badge-profile-img"
                                                    />
                                                </>
                                            )}
                                        </div>
                                        <small className="d-block">
                                            <a
                                                onClick={() =>
                                                    history.push(
                                                        "/employee-profile/" +
                                                        badgeSendingitems.user.id
                                                    )
                                                }
                                            >
                                                {
                                                    badgeSendingitems.user
                                                        .first_name
                                                }{" "}
                                                {badgeSendingitems.user.last_name}
                                            </a>
                                        </small>
                                    </div>
                                )
                            )
                        ) : (
                            <></>
                        )}
                    </div>

                    <div
                        className="list-badge-prview mb-1"
                        style={{
                            backgroundImage: `URL(${backgroundImage})`,
                            backgroundRepeat: "no-repeat",
                            backgroundPosition: "center",
                        }}
                    >
                        <div className="badge-box">
                            <img
                                src={item?.BadgeImage?.BadgeImage}
                                alt=""
                            />
                            <h5 className="font-weight-bolder mt-1">
                                {item?.BadgeImage?.BadgeName}
                            </h5>
                        </div>
                    </div>

                </div>
            </div>
        ))
    )
}

export default SingleBadge;