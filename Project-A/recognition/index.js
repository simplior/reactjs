import { API, Auth, graphqlOperation } from "aws-amplify";
import { React, useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner,
} from "reactstrap";
import thankyoubadge from "../../../src/assets/images/icons/thankyou-badge.png";
import image18 from "../../../src/assets/images/portrait/small/avatar-s-18.jpg";
import images7 from "../../../src/assets/images/portrait/small/avatar-s-7.jpg";
import imageslider2 from "../../../src/assets/images/slider/02.jpg";
import {
  createBadgeAction,
  listBadgeAction,
  listBadgeImagesAction,
} from "../../redux/actions/badge";
import { getUser } from "../../graphql/queries";
import userIcon from "@src/assets/images/avatars/icon_user.png";
import { AmplifyS3Image, theme } from "@aws-amplify/ui-react";
import moment from "moment";
import { Multiselect } from "multiselect-react-dropdown";
import { listRecognitionBadgeImages } from "../../services/custom.queries.service";
import { getCompany } from "./../../redux/actions/company/index";
import { listRecognitionBadges } from "../../services/custom.queries.service";
import { toast } from "react-toastify";
import backgroundImage from "../../../src/assets/images/icons/badge-bg.png";
import { X, Plus, Check } from "react-feather";
import Avatar from "@components/avatar";

const Badge = () => {
  const [ModalToggle, setModalToggle] = useState(false);
  const [NextModalToggle, setNextModalToggle] = useState(false);
  const [PreviewModalToggle, setPreviewModalToggle] = useState(false);
  var [badgeImage, setBadgeImage] = useState(null);
  const [userID, setUserID] = useState("");
  const [userProfileImage, setUserProfileImage] = useState(null);
  const [sendingBadgeUser, setSendingBadgeUser] = useState([]);
  const [description, setDescription] = useState(null);
  const [companyID, setCompanyID] = useState("");
  const [userName, setUserName] = useState("");
  const [img, setImg] = useState();
  const userSelector = useSelector((state) => state.UserReducer).user;
  const companySelector = useSelector((state) => state.CompanyReducer).company;
  const cognitoUser = useSelector((state) => state.UserReducer).cognitoUser;
  const [userListing, setUserListing] = useState([]);
  var [badgeImagesArr, setBadgeImagesArr] = useState([]);
  const [badgeImageID, setBadgeImageID] = useState(null);
  const [badgeImageName, setBadgeImageName] = useState(null);
  const [badgeProcess, setBadgeProcess] = useState(false);
  const [isPublic, setisPublic] = useState(true);
  const [imageSelectedCheck, setImageSelectedCheck] = useState(false);
  const [load, setLoad] = useState(5);
  const [loadProcess, setLoadProcess] = useState(false);
  const [read, setRead] = useState(false);
  const [index1, setIndex1] = useState(null);

  const readmore = (i) => {
    setIndex1(i);
    setRead(!read);
  };

  const ToastComponent = ({ title, color }) => (
    <Fragment>
      <div className="toastify-header pb-0">
        <div className="title-wrapper">
          <Avatar size="sm" color={color} icon={<X size={12} />} />
          <h6 className="toast-title">{title}</h6>
        </div>
      </div>
    </Fragment>
  );

  const ToastComponentSuccess = ({ title, color }) => (
    <Fragment>
      <div className="toastify-header pb-0">
        <div className="title-wrapper">
          <Avatar size="sm" color={color} icon={<Check size={12} />} />
          <h6 className="toast-title">{title}</h6>
        </div>
      </div>
    </Fragment>
  );

  const dispatch = useDispatch();

  const selectsCheckBox = () => {
    setisPublic(!isPublic);
  };

  const selectImageCheckBox = () => {
    setImageSelectedCheck(imageSelectedCheck);
  };

  const handleBadgeImageChange = (
    selectedbadgeImageID,
    selectedBadgeImage,
    selectedBadgeImageName
  ) => {
    setBadgeImageID(selectedbadgeImageID);
    setBadgeImage(selectedBadgeImage);
    setBadgeImageName(selectedBadgeImageName);
  };

  var loggeInUser = JSON.parse(localStorage.getItem("UserData"));
  var loggedInUserId = loggeInUser.username;

  const handleCreateBadge = async (e) => {
    // e.preventDefault();
    setBadgeProcess(true);
    let payload = {
      BadgeImage: badgeImageID,
      sendingBadgeUser: sendingBadgeUser,
      description: description,
      userID: userID,
      companyID: companyID,
      isPublic: isPublic,
    };
    // console.log("badge payload", payload);
    try {
      await dispatch(
        createBadgeAction(payload, userSelector, companySelector, cognitoUser)
      );
      toast.success(
        <ToastComponentSuccess title="Recognition Send!" color="success" />,
        {
          autoClose: 3000,
          hideProgressBar: true,
          closeButton: false,
        }
      );
    } catch (error) {
      toast.error(<ToastComponent title={error.message} color="danger" />, {
        autoClose: 3000,
        hideProgressBar: true,
        closeButton: false,
      });
      console.log("Error in sending Recognition!", error);
    }
    try {
      await dispatch(getCompany());
    } catch (err) {
      console.log(err);
    }
    setSendingBadgeUser([]);
    setDescription(null);
    setBadgeImage(null);
    setBadgeProcess(false);
    setPreviewModalToggle(!PreviewModalToggle);
  };

  useEffect(() => {
    setLoadProcess(true);
    getData();
  }, [companySelector]);

  const getData = async () => {
    let userArr = [];
    var userData = JSON.parse(localStorage.getItem("UserData"));
    var logInUserId = userData.userName;
    if (Object.keys(companySelector).length === 0) {
      await dispatch(getCompany());
    } else {
      let usr = await Auth.currentUserInfo();
      // console.log("userID in badge", usr);
      var usersOfCompany = companySelector.users.items.filter((item) => {
        return item.id !== logInUserId;
      });
      usersOfCompany.map((items) => {
        items.name = `${items.first_name} ${items.last_name}`;
        userArr.push(items);
      });
      // console.log("userArr in badge", userArr);

      let users = userArr.filter((option) => option.id !== usr.username);
      // console.log("users in badge", users);
      setUserListing(users);
    }
    // console.log("USEEFFECT CALLED");
    try {
      await dispatch(listBadgeImagesAction());
    } catch (err) {
      console.log(err);
    }

    try {
      await dispatch(listBadgeAction());
    } catch (err) {
      console.log(err);
    }
    try {
      var userInfo = await API.graphql(
        graphqlOperation(getUser, { id: loggedInUserId })
      );
      // console.log("UserINFO", userInfo);
    } catch (err) {
      console.log(err);
    }

    setUserProfileImage(userInfo.data.getUser.profileImage);

    try {
      await Auth.currentUserInfo().then((user) => {
        if (user !== null) {
          // console.log("user: ", user);
          setUserID(user.attributes.sub);

          setCompanyID(user.attributes["custom:companyID"]);

          setUserName(
            user.attributes["custom:first_name"] +
              " " +
              user.attributes["custom:last_name"]
          );
        } else if (user === null) {
          setUserID("");
        }
      });
    } catch (err) {}
    setLoadProcess(false);
  };

  var badgearr = useSelector((state) => state.badgeReducer).badgearr;
  var badgeimagearr = useSelector((state) => state.badgeReducer).badgeImageArr;
  // console.log("BadgeImageArrrrrr", badgeimagearr);

  return (
    /* <!-- BEGIN: Content--> */
    <div>
      <div className="content-overlay"></div>
      <div className="header-navbar-shadow"></div>
      <div className="content-wrapper">
        <div className="content-header row">
          <div className="content-header-left col-md-9 col-12 mb-2">
            <div className="row breadcrumbs-top">
              <div className="col-12">
                <h2 className="content-header-title float-left mb-0 border-0">
                  Recognition
                </h2>
              </div>
            </div>
          </div>

          <div className="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
            <div className="form-group breadcrumb-right">
              <a
                className="btn btn-primary btn-sm mb-1 waves-effect waves-float waves-light"
                data-toggle="modal"
                data-target="#badges-model"
                onClick={() => setModalToggle(!ModalToggle)}
              >
                Send Recognition
              </a>
            </div>

            <Modal
              size="lg"
              isOpen={ModalToggle}
              toggle={(e) => {
                setBadgeImage(null);
                setModalToggle(!ModalToggle);
              }}
            >
              <ModalHeader
                toggle={(e) => {
                  setBadgeImage(null);
                  setModalToggle(!ModalToggle);
                }}
              >
                Send Recognition
              </ModalHeader>

              <ModalBody>
                <h5>Select Badge</h5>

                <div className="badges-box-wrp">
                  {badgeimagearr.length > 0 ? (
                    badgeimagearr.map((item) =>
                      item.BadgeImage === badgeImage ? (
                        <div
                          className="badge-box selected"
                          onClick={() => {
                            handleBadgeImageChange(
                              item.id,
                              item.BadgeImage,
                              item.BadgeName
                            );
                          }}
                          style={{ cursor: "pointer" }}
                        >
                          <span className="check">
                            <input type="checkbox" checked />
                          </span>

                          <div className="badge-center">
                            <img
                              src={item.BadgeImage}
                              alt=""
                              onChange={selectImageCheckBox}
                            />
                            <h5 className="font-weight-bolder">
                              {item.BadgeName}
                            </h5>
                          </div>
                        </div>
                      ) : (
                        <div
                          className="badge-box"
                          onClick={() => {
                            handleBadgeImageChange(
                              item.id,
                              item.BadgeImage,
                              item.BadgeName
                            );
                          }}
                          style={{ cursor: "pointer" }}
                        >
                          <div className="badge-center">
                            <img
                              src={item.BadgeImage}
                              alt=""
                              onChange={selectImageCheckBox}
                            />
                            <h5 className="font-weight-bolder">
                              {item.BadgeName}
                            </h5>
                          </div>
                        </div>
                      )
                    )
                  ) : (
                    <></>
                  )}
                </div>
              </ModalBody>

              <ModalFooter>
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  data-dismiss="modal"
                  onClick={() => {
                    setModalToggle(!ModalToggle);
                    setBadgeImage(null);
                  }}
                >
                  Cancel
                </button>
                {badgeImage === null ? (
                  <button
                    type="button"
                    className="disabled btn btn-primary"
                    disabled
                  >
                    Next
                  </button>
                ) : (
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      setNextModalToggle(!NextModalToggle);
                      setModalToggle(!ModalToggle);
                    }}
                  >
                    Next
                  </button>
                )}
              </ModalFooter>
            </Modal>

            {/* Next Modal */}

            <Modal
              size="lg"
              isOpen={NextModalToggle}
              toggle={(e) => {
                setDescription(null);
                setNextModalToggle(!NextModalToggle);
              }}
            >
              <ModalHeader
                toggle={(e) => {
                  setDescription(null);
                  setNextModalToggle(!NextModalToggle);
                }}
              >
                Send Recognition
              </ModalHeader>

              <ModalBody>
                <div className="d-flex justify-content-start align-items-center mb-1">
                  {/* <!-- avatar --> */}
                  <div className="avatar mr-1">
                    {userProfileImage === null ? (
                      <img
                        src={userProfileImage === null ? userIcon : img}
                        height="50"
                        width="50"
                      />
                    ) : (
                      <>
                        <AmplifyS3Image
                          className="badge-profile-img"
                          imgKey={userProfileImage}
                          // theme={{
                          //   photoImg: { width: "42px", height: "42px" },
                          // }}
                        />
                      </>
                    )}
                  </div>
                  {/* <!--/ avatar --> */}
                  <div className="profile-user-info">
                    <h6 className="mb-0">{userName}</h6>
                    <small className="text-muted">Send Recognition</small>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="bage-small-preview">
                      <div className="badge-box">
                        <img src={badgeImage} />
                        <h5 className="mt-2">{badgeImageName}</h5>
                      </div>
                    </div>
                  </div>

                  <div className="col-sm-6">
                    <div className="form-group">
                      <label for="credit-card">To</label>
                      <Multiselect
                        placeholder="Search"
                        // invalid={sendingBadgeUser.length == 0}
                        // selectedValues={roles}
                        options={userListing}
                        onRemove={(selectedList, selectedItem, index) => {
                          setSendingBadgeUser(selectedList);
                        }}
                        onSelect={(selectedList, selectedItem, index) =>
                          setSendingBadgeUser(selectedList)
                        } // Function will trigger on select event
                        displayValue="name" // Property name to display in the dropdown options
                        required
                      />
                    </div>

                    <div className="form-group">
                      <label for="exampleFormControlTextarea1">Note</label>
                      <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        placeholder="Write your message here!"
                        onChange={(e) => {
                          setDescription(e.target.value);
                        }}
                      ></textarea>
                    </div>
                  </div>
                </div>
              </ModalBody>

              <ModalFooter>
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  data-dismiss="modal"
                  onClick={() => {
                    setNextModalToggle(!NextModalToggle);
                    setModalToggle(!ModalToggle);
                  }}
                >
                  Back
                </button>
                {description === null ||
                description === "" ||
                sendingBadgeUser.length === 0 ||
                sendingBadgeUser === [] ? (
                  <button
                    type="button"
                    className="disabled btn btn-primary"
                    disabled
                  >
                    Preview
                  </button>
                ) : (
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      if (sendingBadgeUser.length !== 0) {
                        // console.log(
                        //   "sendingBadgeUser",
                        //   sendingBadgeUser.length
                        // );
                        setPreviewModalToggle(!PreviewModalToggle);
                        setNextModalToggle(!NextModalToggle);
                      } else {
                        // console.log(
                        //   "sendingBadgeUser",
                        //   sendingBadgeUser.length
                        // );
                      }
                    }}
                  >
                    Preview
                  </button>
                )}
              </ModalFooter>
            </Modal>

            {/* Preview modal */}
            <Modal
              size="lg"
              isOpen={PreviewModalToggle}
              toggle={(e) => {
                setPreviewModalToggle(!PreviewModalToggle);
              }}
            >
              <ModalHeader
                toggle={(e) => {
                  setPreviewModalToggle(!PreviewModalToggle);
                }}
              >
                Preview Recognition
              </ModalHeader>

              <ModalBody>
                <div className="d-flex justify-content-start align-items-center mb-1">
                  {/* <!-- avatar --> */}
                  <div className="avatar mr-1">
                    {userProfileImage === null ? (
                      <img
                        src={userProfileImage === null ? userIcon : img}
                        height="50"
                        width="50"
                      />
                    ) : (
                      <>
                        <AmplifyS3Image
                          className="badge-profile-img"
                          imgKey={userProfileImage}
                          // theme={{
                          //   photoImg: { width: "42px", height: "42px" },
                          // }}
                        />
                      </>
                    )}
                  </div>
                  {/* <!--/ avatar --> */}
                  <div className="profile-user-info">
                    <h6 className="mb-0">{userName}</h6>
                    <small className="text-muted">Send Recognition</small>
                  </div>
                </div>

                <div className="border p-2 mb-1 r">
                  <div className="text-center mb-2">
                    <small>{userName} sends recognition to</small>
                    <p className="">
                      {sendingBadgeUser
                        .join(", ")
                        .replace(/, ([^,]*)$/, " and $1")}
                    </p>

                    <div
                      className="badge-previw"
                      style={{
                        backgroundImage: `URL(${backgroundImage})`,
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center",
                      }}
                    >
                      <div className="badge-box">
                        <img src={badgeImage} alt="" />

                        <h5 className="font-weight-bolder mt-1">
                          {badgeImageName}
                        </h5>
                      </div>
                    </div>
                  </div>

                  <p className="text-center">{description}</p>
                </div>

                {isPublic === true ? (
                  <div className="custom-control custom-checkbox mb-1">
                    <input
                      className="custom-control-input"
                      id="remember-me"
                      tabindex="3"
                      type="checkbox"
                      checked
                      onChange={selectsCheckBox}
                    />
                    <label className="custom-control-label" for="remember-me">
                      Do you want to share this on Recognition Wall?
                    </label>
                  </div>
                ) : (
                  <div className="custom-control custom-checkbox mb-1">
                    <input
                      className="custom-control-input"
                      tabindex="3"
                      id="remember-me"
                      type="checkbox"
                      onChange={selectsCheckBox}
                    />
                    <label className="custom-control-label" for="remember-me">
                      Do you want to share this on Recognition Wall?
                    </label>
                  </div>
                )}
              </ModalBody>

              <ModalFooter>
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  data-dismiss="modal"
                  onClick={() => {
                    setPreviewModalToggle(!PreviewModalToggle);
                    setNextModalToggle(!NextModalToggle);
                    setSendingBadgeUser([]);
                    setDescription(null);
                  }}
                >
                  Back
                </button>
                {badgeProcess === true ? (
                  <button
                    type="button"
                    className="disabled btn btn-primary"
                    disabled
                    data-dismiss="modal"
                  >
                    <Spinner color="light" size="sm" />
                    Send Card
                  </button>
                ) : (
                  <button
                    type="button"
                    className="btn btn-primary"
                    data-dismiss="modal"
                    onClick={(e) => {
                      // console.log(sendingBadgeUser);
                      handleCreateBadge(e);
                    }}
                  >
                    Send Card
                  </button>
                )}
              </ModalFooter>
            </Modal>
          </div>
        </div>
        <div className="content-body">
          <section>
            <hr className="mb-2 mt-0" />
          </section>
        </div>

        <div className="row">
          {/* <!-- left profile info section --> */}
          <div className="col-lg-3 col-12 order-2 order-lg-1">
            {/* <!-- twitter feed card --> */}
            <div className="card">
              <div className="card-body">
                <h5>Recent</h5>
                {/* <!-- twitter feed --> */}
                <div className="profile-twitter-feed mt-1">
                  <a href="#">Indain</a>
                  <a href="#">#Simplior</a>
                </div>
                <hr />

                <h6 className="mb-0">Followed Hashtags</h6>
                <div className="profile-twitter-feed mt-1">
                  <a href="javascript:void(0)">#design</a>
                  <a href="#">#fasion</a>
                  <a href="#">#web design</a>
                </div>
              </div>
            </div>
            {/* <!--/ twitter feed card --> */}
          </div>
          {/*  <!--/ left profile info section --> */}

          {/* <!-- center profile info section --> */}
          <div className="col-lg-6 col-12 order-1 order-lg-2">
            {badgearr.length > 0 ? (
              badgearr.slice(0, load).map((item, i) => (
                <div className="card profileimagetest">
                  <div className="card-body">
                    <div className="d-flex justify-content-between align-items-start">
                      <div className="d-flex justify-content-start align-items-center mb-1">
                        {/* <!-- avatar --> */}
                        <div className="avatar mr-1">
                          {item.createdByUser.profileImage === null ? (
                            <img
                              src={
                                item.createdByUser.profileImage === null
                                  ? userIcon
                                  : img
                              }
                              height="50"
                              width="50"
                            />
                          ) : (
                            <>
                              <AmplifyS3Image
                                className="badge-profile-img"
                                imgKey={item.createdByUser.profileImage}
                                // theme={{
                                //   photoImg: { width: "42px", height: "42px" },
                                // }}
                              />
                            </>
                          )}
                        </div>
                        {/* <!--/ avatar --> */}
                        <div className="profile-user-info">
                          <h6 className="mb-0">
                            {item.createdByUser.first_name}{" "}
                            {item.createdByUser.last_name}
                          </h6>
                          <small className="text-muted">
                            Send Recognition -{" "}
                            {moment(item.createdAt).fromNow()}
                          </small>
                        </div>
                      </div>
                      <button
                        type="button"
                        className="btn btn-outline-primary btn-sm round waves-effect"
                      >
                        Publish
                      </button>
                    </div>
                    {!read ? (
                      item.description.length > 100 ? (
                        `${item.description.slice(0, 100)}  . . . . `
                      ) : (
                        `${item.description}`
                      )
                    ) : (
                      <p className="card-text" id={`project${i}`}>
                        {item.description}
                      </p>
                    )}
                    {item.description.length > 100 ? (
                      <div className="w-100 text-right mb-1">
                        <a
                          href="#"
                          data-toggle="collapse"
                          href={`#project${i}`}
                          role="button"
                          onClick={() => readmore(i)}
                          aria-expanded="false"
                          aria-controls="collapseExample"
                          className="w-100 text-right mb-1"
                        >
                          {read && i === index1 ? "Read Less" : "Read More"}
                        </a>
                      </div>
                    ) : null}

                    {/* <div className="w-100 text-right mb-1">
                            <a href="#">...see more</a>
                          </div> */}
                    {/* <!-- post img --> */}

                    <div
                      className="list-badge-prview mb-1"
                      style={{
                        backgroundImage: `URL(${backgroundImage})`,
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center",
                      }}
                    >
                      <div className="badge-box">
                        <img src={item.BadgeImage.BadgeImage} alt="" />
                        <h5 className="font-weight-bolder mt-1">
                          {item.BadgeImage.BadgeName}
                        </h5>
                      </div>
                    </div>
                    {/*  <!--/ post img --> */}
                  </div>
                </div>
              ))
            ) : (
              <div style={{ textAlign: "center" }}>
                {loadProcess === true ? (
                  <Spinner color="dark" size="sm">
                    Loading...
                  </Spinner>
                ) : (
                  <h1>No Recognition</h1>
                )}
              </div>
            )}
            {badgearr.length > 5 ? (
              load >= badgearr.length ? (
                <button onClick={() => setLoad(load - 5)}>Load Less</button>
              ) : (
                <button onClick={() => setLoad(load + 5)}>Load More</button>
              )
            ) : (
              <div></div>
            )}
          </div>
          {/* <!--/ center profile info section --> */}

          {/* <!-- right profile info section --> */}
          <div className="col-lg-3 col-12 order-3">
            {/* <!-- suggestion --> */}
            <div className="card">
              <div className="card-body">
                <img src={imageslider2} style={{ width: "100%" }} />
              </div>
            </div>
            {/* <!--/ suggestion --> */}
          </div>
          {/* <!--/ right profile info section --> */}
        </div>
      </div>
    </div>
  );
};

export default Badge;
