const initialState = {
  badgearr: [],
  badgeImageArr: [],
  allBadgeArr: [],
};

const badgeReducer = (state = initialState, action) => {
  // console.log("badge action", action.type);

  switch (action.type) {
    case "LIST_BADGE":
      return { ...state, badgearr: action.data };

    case "LIST_BADGE_IMAGE":
      return { ...state, badgeImageArr: action.data };

    case "LIST_ALL_BADGE":
      return { ...state, allBadgeArr: action.data };

    case "CREATE_BADGE_COMMENT":
      return {
        ...state,
        badgearr: action.data.badgeComment,
        allBadgeArr: action.data.allBadgesComment,
      };

    case "CREATE_BADGE_LIKE":
      return {
        ...state,
        badgearr: action.data.badges,
        allBadgeArr: action.data.allBadges,
      };

    case "DELETE_BADGE_COMMENT":
      return {
        ...state,
        badgearr: action.data.deleteBadgesComment,
        allBadgeArr: action.data.deleteAllBadgesComment,
      };

    case "UPDATE_BADGE_COMMENT":
      return {
        ...state,
        badgearr: action.data.updateBadgeComment,
        allBadgeArr: action.data.updateAllBadgesComment,
      };

    case "LIKED_BADGE_COMMENT":
      return {
        ...state,
        badgearr: action.data.badgeNestedLike,
        allBadgeArr: action.data.allBadgesNestedLike,
      };

    case "DELETE_BADGE_LIKE":
      return {
        ...state,
        badgearr: action.data.deleteBadgeLike,
        allBadgeArr: action.data.deleteAllBadgesLike,
      };

    case "DELETE_BADGE_COMMENT_LIKE":
      return {
        ...state,
        badgearr: action.data.deleteBadgeCommentLike,
        allBadgeArr: action.data.deleteAllBadgesCommentLike,
      };

    default:
      return state;
  }
};

export default badgeReducer;
