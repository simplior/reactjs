import { API, Auth, graphqlOperation } from "aws-amplify";
import EmailService from "../../../services/email.service";
import moment from "moment";

//importing services
import BadgeService from "../../../services/badge.service";
import { listRecognitionBadgeImages } from "../../../services/custom.queries.service";
import NoteService from "../../../services/note.service";

import { badgeEmailTemplate } from "../../../emailTemplates/badgeEmailTemplate";

//badge images action
export const listBadgeImagesAction = () => {
  return async (dispatch) => {
    try {
      var badgeImages = await API.graphql(
        graphqlOperation(listRecognitionBadgeImages)
      );
      var badgeImagesArr = badgeImages.data.listBadgeImages.items;
      // console.log("Arrrrrr", badgeImagesArr);
    } catch (err) {
      console.log(err);
    }

    return dispatch({
      type: "LIST_BADGE_IMAGE",
      data: badgeImagesArr,
    });
  };
};

//creating badge action
export const createBadgeAction = (
  payload,
  userSelector,
  CompanySelector,
  CognitoUser
) => {
  return async (dispatch) => {
    try {
      // console.log("sendingBadgeUser", payload.sendingBadgeUser);
      let BadgeImage = payload.BadgeImage;
      let badgeUrl = payload.badgeUrl;
      let badgeImageName = payload.badgeImageName;

      if (payload.companyID === undefined) {
        payload.companyID = localStorage.getItem("companyID");
      }
      let badgePayload = {
        BadgeImg: BadgeImage,
        userID: payload.userID,
        description: payload.description,
        companyID: payload.companyID,
        isPublic: payload.isPublic,
        badgeUrl: badgeUrl,
      };

      let badgeEmailPayload = {
        BadgeName: badgeImageName,
      };
      // console.log("BadgePAYLOAD", badgePayload);
      try {
        var createdBadge = await BadgeService.createBadge(badgePayload);
        // console.log("CREATED BADGE ACTION", createdBadge);
      } catch (err) {
        console.log(err);
      }
      let BadgeId = createdBadge.data.createBadge.id;
      var BadgeUrl = createdBadge.data.createBadge.badgeUrl;
      var BadgeImageName = createdBadge.data.createBadge.BadgeImage.BadgeName;
      // let sendingBadgeUserID = user.data.listUsers.items[0].id
      let UserArr = [];
      let userWithName = [];
      for (let index = 0; index < payload.sendingBadgeUser.length; index++) {
        const element = payload.sendingBadgeUser[index];
        UserArr.push(element.email);
        userWithName.push(`${element.first_name} ${element.last_name}`);
        let userBadgePayload = {
          userID: element.id,
          badgeID: BadgeId,
        };
        // console.log("userbsdgeeeee", userBadgePayload);
        var createdUserBadge = BadgeService.createUserBadge(userBadgePayload);
        // console.log("createdUserBadge", createdUserBadge);
      }
      try {
        var loggedInUserEmail = CognitoUser.attributes.email;
        let emailMessage = `${CognitoUser.attributes["custom:first_name"]} ${CognitoUser.attributes["custom:last_name"]}`;
        let emailPayload = {
          emailMessage: emailMessage,
          userWithName: userWithName,
          BadgeUrl: BadgeUrl,
          BadgeImageName: BadgeImageName,
        };
        var emailBody = badgeEmailTemplate(emailPayload);
        // console.log("UserArr", typeof UserArr);
        try {
          // UserArr=UserArr.toString()
          sendNotificationAllUser(
            createdBadge,
            payload.sendingBadgeUser,
            userWithName,
            CognitoUser,
            CompanySelector
          );
          let emailJson = {
            body: emailBody,
            from: loggedInUserEmail,
            subject: "New Badge Recieved",
            to: [UserArr], //["webtech@yopmail.com","yash@simplior.com","pratik@webtech.com"],
          };
          // console.log(emailJson);

          let sendEmail = EmailService.sendEmailNotification(emailJson);
          // console.log("sendEmail :>> ", sendEmail);
        } catch (error) {
          console.log("error :>> ", error);
        }
      } catch (err) {
        console.log(err);
      }
      try {
        var listBadges = await BadgeService.listBadges();
        // console.log("LISTBADGES", listBadges.data.listBadges.items);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      console.log(err);
    }
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          payload.userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "LIST_BADGE",
      data: badgeArr,
    });
  };
};

//listing employee badges action
export const listBadgeAction = (CognitoUser) => {
  return async (dispatch) => {
    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "LIST_BADGE",
      data: badgeArr,
    });
  };
};

//listing employee badges (which are public) action
export const listAllBadgesAction = (CognitoUser) => {
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "LIST_ALL_BADGE",
      data: listBadges.data.listBadges.items,
    });
  };
};

//comment action
export const addBadgesCommentAction = (payload, CognitoUser) => {
  // console.log("Comment badge action payload", payload);
  return async (dispatch) => {
    const cognitoInfo = await Auth.currentAuthenticatedUser();
    var companyID = cognitoInfo.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      var createdComment = BadgeService.commentBadges(payload);
      // console.log("CREATED BADGE COMMENT", createdComment);
    } catch (err) {
      console.log(err);
    }

    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = cognitoInfo.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });

    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = cognitoInfo.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "CREATE_BADGE_COMMENT",
      data: {
        badgeComment: badgeArr,
        allBadgesComment: listBadges.data.listBadges.items,
      },
    });
  };
};

//delete comment action
export const deleteBadgeCommentAction = (payload, CognitoUser) => {
  // console.log("DeleteBadgeCommentACTION", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      let deletedComment = BadgeService.deleteBadgesComment(payload);
    } catch (err) {
      console.log(err);
    }
    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });

    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "DELETE_BADGE_COMMENT",
      data: {
        deleteBadgesComment: badgeArr,
        deleteAllBadgesComment: listBadges.data.listBadges.items,
      },
    });
  };
};

//edit or update comment action
export const updateBadgeCommentAction = (payload, CognitoUser) => {
  // console.log("DeleteBadgeCommentACTION", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      let updatedComment = BadgeService.updateCommentBadge(payload);
    } catch (err) {
      console.log(err);
    }

    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });

    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "UPDATE_BADGE_COMMENT",
      data: {
        updateBadgeComment: badgeArr,
        updateAllBadgesComment: listBadges.data.listBadges.items,
      },
    });
  };
};

//notification fuction
const sendNotificationAllUser = async (
  badgeDetails,
  userArr,
  badgeAssignUserWithName,
  CognitoUser,
  CompanySelector
) => {
  let badgeInformation = badgeDetails.data.createBadge;
  let name = badgeAssignUserWithName.join();
  // Notification Dynamic message
  let NoteMessage = `${CognitoUser.attributes["custom:first_name"]} ${CognitoUser.attributes["custom:last_name"]} sends badge to you`;
  for (let index = 0; index < userArr.length; index++) {
    const element = userArr[index];
    if (CognitoUser.username !== element.id) {
      let createNoteObj = {
        note: NoteMessage,
        type: "Badge",
        notificationDate: moment().toISOString(),
        currentStatus: "CREATED",
        assignedToUserID: element.id,
        assignedByUserID: CognitoUser.username,
      };
      let createNotification = await NoteService.createNote(createNoteObj);
      // console.log("is public false ", createNotification);
    }
  }
  let notapi = await NoteService.publishNotification({
    assignedToUserID: "79781008-f268-4249-bf8a-709a84f2bcb4",
    message: "badge",
  });
  console.log("notapi", notapi);
};

//like action
export const likeBadgesAction = (payload, CognitoUser) => {
  // console.log("LIKEBADGEACTION", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      var likedBadge = BadgeService.likeBadges(payload);
      // console.log("LIKEDBADGE", likedBadge);
    } catch (err) {
      console.log(err);
    }
    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "CREATE_BADGE_LIKE",
      data: { badges: badgeArr, allBadges: listBadges.data.listBadges.items },
    });
  };
};

//delete badge like action
export const deleteBadgeLikeAction = (payload, CognitoUser) => {
  // console.log("deletebadge like action calledd", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      let deletedBadgeLike = await BadgeService.deleteBadgeLike(payload);
      // console.log("Deleted LIKE", deletedBadgeLike);
    } catch (err) {
      console.log(err);
    }

    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });

    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "DELETE_BADGE_LIKE",
      data: {
        deleteBadgeLike: badgeArr,
        deleteAllBadgesLike: listBadges.data.listBadges.items,
      },
    });
  };
};

//nested comment LIKE action
export const badgeCommentLikeAction = (payload, CognitoUser) => {
  // console.log("LikedBadgeCommentACTION", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      var likedBadgeComment = BadgeService.likeBadgeComment(payload);
    } catch (err) {
      console.log(err);
    }
    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "LIKED_BADGE_COMMENT",
      data: {
        badgeNestedLike: badgeArr,
        allBadgesNestedLike: listBadges.data.listBadges.items,
      },
    });
  };
};

//delete nested comment LIKE action
export const deleteBadgeCommentLikeAction = (payload, CognitoUser) => {
  // console.log("deletebadge like action calledd", payload);
  return async (dispatch) => {
    var companyID = CognitoUser.attributes["custom:companyID"];
    if (companyID === undefined) {
      companyID = localStorage.getItem("companyID");
    }
    try {
      let deletedBadgeCommentLike =
        BadgeService.deleteBadgeCommentLike(payload);
      // console.log("Deleted Comment LIKE", deletedBadgeCommentLike);
    } catch (err) {
      console.log(err);
    }

    try {
      var listBadges = await BadgeService.listBadges();
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let badgeArr = [];
    for (var i = 0; i < listBadges.data.listBadges.items.length; i++) {
      for (
        var j = 0;
        j < listBadges.data.listBadges.items[i].BadgeSending.items.length;
        j++
      ) {
        if (
          listBadges.data.listBadges.items[i].BadgeSending.items[j].userID ===
          userID
        ) {
          badgeArr.push(listBadges.data.listBadges.items[i]);
        }
      }
    }
    badgeArr.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });

    try {
      var listBadges = await BadgeService.listAllBadges(companyID);
      // console.log("LISTBADGES", listBadges.data.listBadges.items);
    } catch (err) {
      console.log(err);
    }
    var userID = CognitoUser.username;
    let allBadgeArr = [];

    listBadges.data.listBadges.items.sort((a, b) => {
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      if (a.createdAt > b.createdAt) {
        return -1;
      }

      return 0;
    });
    return dispatch({
      type: "DELETE_BADGE_COMMENT_LIKE",
      data: {
        deleteBadgeCommentLike: badgeArr,
        deleteAllBadgesCommentLike: listBadges.data.listBadges.items,
      },
    });
  };
};
